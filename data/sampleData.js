/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var BW1 = "<chart caption='Visits to a Website' subCaption='In three years' useRoundEdges='1'>\n\
<categories>\n\
    <category label='2008' />\n\
    <category label='2009' />\n\
    <category label='2010' />\n\
</categories>\n\
<dataset seriesName='January' lowerBoxColor='019FAA' upperboxColor='D4DFFF'>\n\
    <set value='60,133,35,67,89,137,259' />\n\
    <set value='139,197,175,114,39,67,191' />\n\
    <set value='200,107,197,239,53,26,97' />\n\
</dataset>\n\
<dataset seriesName='February' lowerBoxColor='2A5F55' upperboxColor='D47F00'>\n\
    <set value='160,303,125,137,69,107,39' />\n\
    <set value='139,167,255,74,59,187,151' />\n\
    <set value='160,137,57,209,153,126,277' />\n\
</dataset>\n\
</chart>",
BW2 = "<chart caption='Visits to a Website' subCaption='In three years' useroundedges='1' plotspacepercent='70'>\n\
<categories>\n\
  <category label='2008' />\n\
  <category label='2009' />\n\
  <category label='2010' />\n\
</categories>\n\
<dataset seriesName='January'  lowerboxcolor='006ECB' upperboxcolor='9071CD'>\n\
    <set value='160,133,35,67,89,107,59' />\n\
    <set value='239,67,75,114,169,97,191' />\n\
    <set value='200,107,197,289,53,36,97' />\n\
</dataset>\n\
<dataset seriesName='Febuary' upperboxcolor='73D9F5' lowerboxcolor='0084A3'>\n\
    <set value='160,303,125,137,169,107,89' />\n\
    <set value='139,167,255,124,99,187,151' />\n\
    <set value='60,137,117,219,153,126,277' />\n\
</dataset>\n\
</chart>",
    BW3 = "<chart caption='Test scores of class in different subjects' showMean='1'>\n\
<categories>\n\
  <category label='Maths' />\n\
  <category label='Physics' />\n\
  <category label='Chemistry' />\n\
  <category label='History' />\n\
  <category label='English' />\n\
</categories>\n\
<dataset seriesName='Scores'>\n\
    <set value='35, 78, 91, 42, 56, 66, 71, 46' />\n\
    <set value='85, 68, 70, 30, 78, 83, 45, 54' />\n\
    <set value='70, 33, 56, 67, 60, 41, 60, 70' />\n\
    <set value='72, 31, 61, 65, 67, 45, 53, 51' />\n\
    <set value='31, 41, 34, 45, 67, 69, 78, 71' />\n\
</dataset>\n\
</chart>",
    BW4 = "<chart caption='Weekly Temperature' subCaption='In a month' showSd='1' showMD='1' showQD='1' SDIconColor='FF9F55' MDIconColor='7F00FF' QDIconCOlor='FFFF00' showValues='0' useRoundEdges='1' showzeroplanevalue='0' plotSpacePercent='75' >\n\
<categories>\n\
      <category label='2005' />\n\
      <category label='2006' />\n\
      <category label='2007' />\n\
</categories>\n\
<dataset seriesName='January' lowerBoxColor='CCCCFF ' upperboxColor='7F9FFF' mediancolor='FF0000' upperWhiskerColor='000080' lowerWhiskerColor='D41FAA'>\n\
   <set value='-6,-3,13,17,9,7.7,10.3' outliers='30, -18' />\n\
   <set value='-13,0.6,21.9,15.4,19,17,41'/>\n\
   <set value='20,10,27,23,3,46,12' outliers='43, -13'/>\n\
</dataset>\n\
</chart>",
    BW5 = "<chart caption='Employee age across different departments in the company' sshowValues='0' showMean='1' outlierIconShape='spoke' outlierIconSides='8' rotateLabels='1' slantLabels='1' yAxisMaxValue='80' showMedianValues='0'>\n\
<categories>\n\
  <category label='Development' />\n\
  <category label='Marketing' />\n\
  <category label='Sales' />\n\
  <category label='QA' />\n\
  <category label='Finance' />\n\
  <category label='Admin' />\n\
</categories>\n\
<dataset seriesName='Age' >\n\
    <set value='23, 25, 19, 32, 36, 29, 31, 29' outliers='2'/>\n\
    <set value='21, 23, 24, 35, 47'/>\n\
    <set value='33, 37, 39, 29, 52, 47, 32, 35, 32, 34, 35, 31, 29, 38, 42' outliers='67, 72'/>\n\
    <set value='24, 27, 29, 21, 25'/>\n\
    <set value='31, 36, 37, 31, 29, 32, 34'/>\n\
    <set value='21, 20, 26, 27, 30'/>\n\
</dataset>\n\
</chart>",
    BW6 = '<chart caption="Cost per conversion for keywords on Adwords - Q3 v Q4 2010" useRoundEdges="1" yAxisMaxValue="8" showValues="0" numberPrefix="$">\n\
<categories>\n\
    <category label="Brand" />\n\
    <category label="Primary" />\n\
    <category label="Secondary" />\n\
    <category label="Long Tail" />\n\
</categories>\n\
<dataset seriesName="Q3 2010" >\n\
    <set value="2.4, 2.4, 3.6, 1.8, 2.1, 3.3, 2.4" />\n\
    <set value="2.7, 3.5, 2.9, 1.3, 4.7, 5.4, 6.5, 3.9, 4.4" />\n\
    <set value="2.4, 3.3, 2.5, 3.6, 2.2" />\n\
    <set value="1.2, 2.4, 1.6, 0.8" />\n\
</dataset>\n\
<dataset seriesName="Q4 2010" >\n\
    <set value="2.3, 2.2, 3.8, 2.0, 2.4, 3.0, 2.2" />\n\
    <set value="2.2, 3.0, 2.5, 1.2, 5.4" />\n\
    <set value="2.5, 3.2, 2.6, 3.2" />\n\
    <set value="1.4, 2.5, 1.6, 1.0" />\n\
</dataset>\n\
</chart>',
BW7 = '<chart caption="Fuel efficiency test results of cars by manufacturer (in mpg)" showValues="0" showMean="1" numberSuffix=" mpg">\n\
<categories>\n\
    <category label="Honda" />\n\
    <category label="Toyota" />\n\
    <category label="Volkswagen" />\n\
    <category label="Ford" />\n\
</categories>\n\
<dataset seriesName="City" >\n\
    <set value="17.4, 27.6, 25.6, 23.5, 28.2, 20.4, 25.8" />\n\
    <set value="22.3, 22.6, 24.3, 32.3, 22, 16.3, 26.6, 23.5, 13.8, 16.5" />\n\
    <set value="22.3, 20.7, 30, 16.1, 30.4" />\n\
    <set value="19.1, 32.7, 32.7, 31.6, 31.1" />\n\
</dataset>\n\
<dataset seriesName="Highway" >\n\
    <set value="27.6, 31.6, 30, 29, 29.9, 24.8, 29.9" />\n\
    <set value="31.1, 28.2, 28.9, 39.1, 29.2, 18.4, 29.9, 29.2, 20.7, 24.2" />\n\
    <set value="29.9, 24.2, 37.5, 18.4, 35" />\n\
    <set value="25.1, 40, 40.8, 37.5, 39.4" />\n\
</dataset>\n\
</chart>',
    BW8 = '<chart caption="Outbound bandwidth usage per day (in GB)" showMedianValues="0" rotateLabels="1" slantLabels="1" numberSuffix=" GB">\n\
<categories>\n\
    <category label="Jan" />\n\
    <category label="Feb" />\n\
    <category label="Mar" />\n\
    <category label="Apr" />\n\
    <category label="May" />\n\
    <category label="Jun" />\n\
    <category label="Jul" />\n\
    <category label="Aug" />\n\
    <category label="Sep" />\n\
    <category label="Oct" />\n\
    <category label="Nov" />\n\
    <category label="Dec" />\n\
</categories>\n\
<dataset>\n\
    <set value="4, 5, 4, 4, 7, 4, 5, 4, 11, 4, 4, 5, 4, 4, 4, 4, 5, 4, 13, 4, 4, 5, 4, 4, 4, 4, 5, 4, 4, 4, 5"/>\n\
    <set value="4, 5, 4, 2, 4, 4, 5, 4, 4, 4, 4, 5, 4, 4, 4, 4, 7, 4, 4, 4, 4, 5, 4, 4, 4, 4, 5, 4, 4"/>\n\
    <set value="4, 5, 4, 4, 2, 4, 5, 3, 2, 3, 2, 5, 4, 4, 11, 4, 5, 15, 4, 4, 4, 5, 4, 12, 4, 4, 5, 4, 4, 4, 5"/>\n\
    <set value="4, 5, 4, 2, 4, 4, 5, 4, 2, 2, 4, 5, 3, 4, 3, 4, 5, 2, 3, 4, 4, 5, 4, 9, 4, 4, 5, 4, 4, 4"/>\n\
    <set value="2, 2, 3, 2, 3, 4, 9, 7, 4, 11, 4, 11, 4, 4, 4, 11, 5, 4, 4, 4, 4, 5, 4, 4, 4, 4, 5, 4, 4, 4, 5"/>\n\
    <set value="2, 3, 4, 4, 4, 5, 5, 4, 7, 4, 3, 5, 4, 2, 4, 4, 7, 9, 4, 11, 13, 13, 4, 4, 4, 4, 5, 4, 4, 4"/>\n\
    <set value="4, 5, 4, 4, 4, 4, 5, 4, 4, 6, 6, 5, 8, 4, 6, 4, 5, 4, 7, 11, 4, 5, 4, 4, 4, 4, 5, 4, 4, 4, 5"/>\n\
    <set value="4, 5, 6, 6, 4, 4, 5, 7, 4, 9, 4, 5, 4, 9, 4, 4, 5, 6, 4, 4, 9, 5, 4, 11, 4, 4, 5, 11, 4, 4"/>\n\
    <set value="4, 5, 4, 6, 4, 6, 5, 4, 3, 4, 4, 5, 4, 7, 4, 8, 5, 4, 8, 4, 4, 5, 9, 4, 8, 4, 5, 9, 4, 4, 5"/>\n\
    <set value="4, 5, 4, 11, 4, 4, 5, 4, 4, 9, 4, 5, 4, 4, 11, 4, 5, 4, 4, 4, 4, 5, 12, 4, 4, 4, 5, 14, 4, 4"/>\n\
    <set value="4, 5, 4, 7, 10, 4, 5, 12, 4, 4, 12, 5, 4, 4, 4, 4, 5, 12, 4, 4, 4, 5, 4, 2, 2, 4, 5, 4, 4, 4, 5"/>\n\
    <set value="4, 7, 4, 3, 4, 8, 5, 4, 4, 4, 4, 5, 4, 4, 4, 4, 5, 4, 4, 4, 4, 5, 3, 2, 3, 2, 2, 2, 2, 2, 2"/>\n\
</dataset>\n\
</chart>',
BW9 = '<chart caption="IMDB rating of all movies by director" showValues="0" showMean="1" rotateLabels="1" slantLabels="1">\n\
<categories>\n\
    <category label="Quentin Tarantino" />\n\
    <category label="Christopher Nolan" />\n\
    <category label="John Hughes" />\n\
    <category label="Martin Scorsese" />\n\
    <category label="Peter Jackson" />\n\
</categories>\n\
<dataset seriesName="Ratings" >\n\
    <set value="8.4, 8.2, 8.4, 7.1, 9.0, 7.9, 7.2, 7.9" />\n\
    <set value="9.0, 8.9, 8.7, 8.3" />\n\
    <set value="7.9, 7.9, 7.0, 6.4, 7.5, 7.0, 5.5" />\n\
    <set value="8.8, 8.0, 8.5, 8.6, 8.1, 7.4, 7.2, 8.4" />\n\
    <set value="7.6, 6.6, 8.7, 8.8, 8.8, 7.6, 7.1" />\n\
</dataset>\n\
</chart>',
BW10 = "<chart caption='Number of yellow cards per match in World Cup - 2010 v 2006' subcaption='(The chart uses random data for demonstration purposes only)' showValues='0' numDivLines='3'>\n\
<categories>\n\
  <category label='Brazil' />\n\
  <category label='Spain' />\n\
  <category label='Holland' />\n\
  <category label='Germany' />\n\
  <category label='England' />\n\
  <category label='Italy' />\n\
</categories>\n\
<dataset seriesName='2010' upperBoxColor='1D3D64' lowerBoxColor='3874B0'>\n\
    <set value='1, 3, 4, 1, 5, 6' />\n\
    <set value='1, 2, 3, 1, 5, 1, 1, 3' />\n\
    <set value='5, 4, 2, 2, 1, 3, 2, 5' />\n\
    <set value='1, 1, 3, 2, 1, 4, 2, 1' />\n\
    <set value='4, 3, 4, 5, 2, 1' />\n\
    <set value='4, 3, 4, 5, 2, 1' />\n\
</dataset>\n\
<dataset seriesName='2006' upperBoxColor='159CDC' lowerBoxColor='BEDAE8'>\n\
    <set value='2, 2, 3, 4, 1, 5, 6' />\n\
    <set value='1, 2, 3, 1, 4' />\n\
    <set value='1, 3, 4, 1, 2, 4' />\n\
    <set value='2, 1, 2, 4, 1, 4, 2, 1' />\n\
    <set value='1, 2, 4, 3, 2' />\n\
    <set value='1, 3, 3, 4, 2, 1, 1' />\n\
</dataset>\n\
</chart>",
        BW11 = "<chart caption='Yearly Mean Visits' subCaption='(showing rise and fall through connecting line)' showValues='0' showMean='1' MeanIconCOlor='F66B27' MeanIconSides='16' MeanIconRadius='10' drawMeanConnector='1' plotSpacePercent='70' numDivLines='0'>\n\
<categories>\n\
      <category label='2008' />\n\
      <category label='2009' />\n\
      <category label='2010' />	 \n\
</categories>\n\
<dataset seriesName='January'  upperBoxColor='ECF2F7' lowerBoxColor='F6F3EB'>\n\
    <set value='60,133,35,67,89,137,259' />\n\
	<set value='139,197,175,114,39,67,191' />\n\
	<set value='200,107,197,239,53,26,97' />\n\
</dataset>\n\
<dataset seriesName='Febuary' upperBoxColor='EBEFDF' lowerBoxColor='FFF7F2' >\n\
    <set value='160,203,125,137,69,107,39'  />\n\
	<set value='139,167,255,74,59,187,151' />\n\
	<set value='160,137,57,209,153,126,177' />\n\
</dataset>\n\
<styles>\n\
<definition>\n\
<style name='fontstyle' type='Font' color='000000'/>\n\
</definition>\n\
<application>\n\
<apply toObject='DATAVALUES' styles='fontstyle'/>\n\
</application>\n\
</styles>\n\
</chart>",
sp1 = "<chart bgColor='f1f1f1' canvasbgcolor='1D8BD1,FFFFFF' canvasbgalpha='60' canvasbgangle='270' outcnvBaseFontColor='1D8BD1' \n\
caption='Fruit Production for March' subCaption='(in Millions)' yaxisname='Quantity' xaxisname='Fruit' alternateHGridAlpha='30' \n\
alternateHGridColor='FFFFFF' canvasBorderThickness='1' canvasBorderColor='114B78' baseFontColor='1D8BD1' hoverCapBorderColor='114B78'\n\
 hoverCapBgColor='E7EFF6' plotGradientColor='DCE6F9' plotFillAngle='90' plotFillColor='1D8BD1' plotfillalpha='80' showAnchors='0' canvaspadding='20' \n\
plotFillRatio='10,90' showPlotBorder='1' plotborderthickness='1' divlinecolor='FFFFFF' divlinealpha='60' numberSuffix='M'>\n\
  <set label='Orange' value='23'/>\n\
  <set label='Apple' value='12'/>\n\
  <set label='Banana' value='17'/>\n\
  <set label='Mango' value='14'/>\n\
  <set label='Litchi' value='12'/>\n\
<styles>\n\
	<definition>\n\
		<style name='Bevel' type='bevel' distance='2' blurx='4' blury='1'/>\n\
		<style name='DataValues' type='font' borderColor='1D8BD1' bgColor='1D8BD1' color='FFFFFF' />\n\
	</definition>\n\
	<application>\n\
		<apply toObject='DATAPLOT' styles='Bevel' />\n\
		<apply toObject='DATAVALUES' styles='DataValues' />\n\
        </application>\n\
</styles>\n\
</chart>",
sp2 = "<chart palette='3' caption='Monthly Sales Summary' subcaption='For the year 2006' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' numberPrefix='$' showValues='0' >\n\
	<set label='Jan' value='17400' />\n\
	<set label='Feb' value='19800' />\n\
	<set label='Mar' value='21800' />\n\
	<set label='Apr' value='23800' />\n\
	<set label='May' value='29600' />\n\
	<set label='Jun' value='27600' />\n\
	<set label='Jul' value='31800' />\n\
	<set label='Aug' value='39700' />\n\
	<set label='Sep' value='32800' />\n\
	<set label='Oct' value='21900' />\n\
	<set label='Nov' value='32900' />\n\
	<set label='Dec' value='39800' />\n\
</chart>",
 sp3 = '<chart palette="4" caption="Sales Comparison" numdivlines="4" lineThickness="2" showValues="0" formatNumberScale="1" decimals="1" anchorRadius="2" numberPrefix="$"  yAxisMinValue="800000" shadowAlpha="50">\n\
<categories>\n\
  <category label="Jan" />\n\
  <category label="Feb" />\n\
  <category label="Mar" />\n\
  <category label="Apr" />\n\
  <category label="May" />\n\
  <category label="Jun" />\n\
  <category label="Jul" />\n\
  <category label="Aug" />\n\
  <category label="Sep" />\n\
  <category label="Oct" />\n\
  <category label="Nov" />\n\
  <category label="Dec" />\n\
  </categories>\n\
<dataset seriesName="Current Year" color="A66EDD" anchorBorderColor="A66EDD" anchorRadius="4">\n\
  <set value="1127654" />\n\
  <set value="1226234" />\n\
  <set value="1299456" />\n\
  <set value="1311565" />\n\
  <set value="1324454" />\n\
  <set value="1357654" />\n\
  <set value="1296234" />\n\
  <set value="1359456" />\n\
  <set value="1391565" />\n\
  <set value="1414454" />\n\
  <set value="1671565" />\n\
  <set value="1134454" />\n\
  </dataset>\n\
<dataset seriesName="Previous Year" color="F6BD0F" anchorBorderColor="F6BD0F" anchorRadius="4">\n\
  <set value="927654" />\n\
  <set value="1126234" />\n\
  <set value="999456" />\n\
  <set value="1111565" />\n\
  <set value="1124454" />\n\
  <set value="1257654" />\n\
  <set value="1196234" />\n\
  <set value="1259456" />\n\
  <set value="1191565" />\n\
  <set value="1214454" />\n\
  <set value="1371565" />\n\
  <set value="1434454" />\n\
  </dataset>\n\
<styles>\n\
	<definition>\n\
	<style name="MyXScaleAnim" type="ANIMATION" duration="0.7" start="0" param="_xScale" />\n\
<style name="MyYScaleAnim" type="ANIMATION" duration="0.7" start="0" param="_yscale" />\n\
<style name="MyAlphaAnim" type="ANIMATION" duration="0.7" start="0" param="_alpha" />\n\
	</definition>\n\
<application>\n\
        <apply toObject="DIVLINES" styles="MyXScaleAnim,MyAlphaAnim" />\n\
        <apply toObject="HGRID" styles="MyYScaleAnim,MyAlphaAnim" />\n\
	</application>\n\
</styles>\n\
</chart>',
 MAL1 = "<chart palette='2' caption='CPU Usage' subcaption='(Last 10 Hours)' xAxisName='Time' showValues='0' divLineAlpha='100' numVDivLines='4' vDivLineAlpha='0' showAlternateVGridColor='1' alternateVGridAlpha='5' canvasPadding='0' labelDisplay='ROTATE'>\n\
   <categories>\n\
      <category label='10:00' />\n\
      <category label='11:00' />\n\
      <category label='12:00' />\n\
      <category label='13:00' />\n\
      <category label='14:00' />\n\
      <category label='15:00' />\n\
      <category label='16:00' />\n\
      <category label='17:00' />\n\
      <category label='18:00' />\n\
      <category label='19:00' />\n\
   </categories>\n\
   <axis title='CPU Usage' titlePos='left' tickWidth='10' divlineisdashed='1' numberSuffix='%'>\n\
      <dataset seriesName='CPU 1' lineThickness='3' color='CC0000'>\n\
         <set value='16' />\n\
         <set value='19' />\n\
         <set value='16' />\n\
         <set value='17' />\n\
         <set value='23' />\n\
         <set value='23' />\n\
         <set value='15' />\n\
         <set value='14' />\n\
         <set value='19' />\n\
         <set value='21' />\n\
      </dataset>\n\
	<dataset seriesName='CPU 2' lineThickness='3' color='0372AB'>\n\
         <set value='12' />\n\
         <set value='12' />\n\
         <set value='9' />\n\
         <set value='9' />\n\
         <set value='11' />\n\
         <set value='13' />\n\
         <set value='16' />\n\
         <set value='14' />\n\
         <set value='16' />\n\
         <set value='11' />\n\
      </dataset>\n\
   </axis>\n\
   <axis title='PF Usage' axisOnLeft='0' titlePos='right' numDivLines='4' tickWidth='10' divlineisdashed='1' formatNumberScale='1' defaultNumberScale=' MB' numberScaleUnit='GB' numberScaleValue='1024'>\n\
      <dataset seriesName='PF Usage'>\n\
         <set value='696' />\n\
         <set value='711' />\n\
         <set value='636' />\n\
         <set value='671' />\n\
         <set value='1293' />\n\
         <set value='789' />\n\
         <set value='793' />\n\
         <set value='993' />\n\
         <set value='657' />\n\
         <set value='693' />\n\
      </dataset>\n\
   </axis>\n\
   <axis title='Processes' titlepos='RIGHT' axisOnLeft='0' numDivLines='5' tickWidth='10' divlineisdashed='1' >\n\
      <dataset seriesName='Processes'>\n\
         <set value='543' />\n\
         <set value='511' />\n\
         <set value='536' />\n\
         <set value='449' />\n\
         <set value='668' />\n\
         <set value='588' />\n\
         <set value='511' />\n\
         <set value='536' />\n\
         <set value='449' />\n\
         <set value='668' />\n\
      </dataset>\n\
   </axis>\n\
</chart>";



var sa4 = '';
var sa3 = '';
var sa2 = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
    <categories>\n\
        <category label='Jan' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <category label='May' />\n\
        <category label='Jun' />\n\
        <category label='Jul' />\n\
        <category label='Aug' />\n\
        <category label='Sep' />\n\
        <category label='Oct' />\n\
        <category label='Nov' />\n\
        <category label='Dec' />\n\
    </categories>\n\
    <dataset seriesName='2006'>\n\
        <set value='27400' />\n\
        <set value='26000'/>\n\
        <set value='25800' />\n\
        <set value='26800' />\n\
        <set value='' />\n\
        <set value='31800' />\n\
        <set value='' />\n\
        <set value='36700' />\n\
        <set value='29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\
    </dataset>\n\
    <dataset seriesName='2005'>\n\
        <set value='10000'/>\n\
        <set value='11500'/>\n\
        <set value=''/>\n\
        <set value='10000'/>\n\
        <set value='12333' />\n\
        <set value='9800' />\n\
        <set value='11800' />\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
    </dataset>\n\
</chart>";

var sa1 = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
    <categories>\n\
        <category label='Jan' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <categorys label='Jul' />\n\
        <categorys label='Aug' />\n\
        <categorys label='Sep' />\n\
        <categorys label='Oct' />\n\
        <categorys label='Nov' />\n\
        <categorys label='Dec' />\n\
    </categories>\n\
    <dataset seriesName='2006'>\n\
        <set value='27400' />\n\
        <set value=''/>\n\
        <set value='25800' />\n\
        <set value='26800' />\n\
        <set value='29600' />\n\
        <set value='31800' />\n\
        <set value='' />\n\
        <set value='36700' />\n\
        <set value='29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\
    </dataset>\n\
    <dataset seriesName='2005'>\n\
        <set value='10000'/>\n\
        <set value='11500'/>\n\
        <set value='12333'/>\n\
        <set value=''/>\n\
        <set value='12333' />\n\
        <set value='9800' />\n\
        <set value='11800' />\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
    </dataset>\n\
</chart>";



    var ss1 = "<chart caption='Monthly' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
       <set label='Jan' value='420000' />\n\
       <set label='Feb' value='910000' />\n\
       <set label='Mar' value='-720000' />\n\
       <set label='Apr' value='550000' />\n\
       <set label='May' value='810000' />\n\
       <set label='Jun' value='510000' />\n\
       <vline linePosition='1' />\n\
       <set label='Jul' value='680000' />\n\
       <set label='Aug' value='620000' />\n\
       <set label='Sep' value='610000' />\n\
       <set label='Oct' value='490000' />\n\
       <set label='Nov' value='530000' />\n\
       <set label='Dec' value='330000' />\n\
       <trendLines>\n\
          <line startValue='700000' istrendzone='1' valueOnRight='1' toolText='AYAN' endValue='900000' color='009933' displayvalue='Target' showontop = '1' thickness='5'/>\n\
       </trendLines>\n\
       <styles>\n\
          <definition>\n\
             <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
          </definition>\n\
          <application>\n\
             <apply toObject='Canvas' styles='CanvasAnim' />\n\
          </application>\n\
       </styles>\n\
    </chart>",

    ss2 = "<chart caption='Monthly' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
       <set label='Jan' value='420000' />\n\
       <set label='Feb' value='910000' />\n\
       <set label='Mar' value='' />\n\
       <set label='Apr' value='550000' />\n\
       <set label='May' value='810000' />\n\
       <set label='Jun' value='510000' />\n\
       <set label='Jul' value='680000' />\n\
       <set label='Aug' value='' />\n\
       <set label='Sep' value='610000' />\n\
       <set label='Oct' value='490000' />\n\
       <set label='Nov' value='530000' />\n\
       <set label='Dec' value='330000' />\n\
       <trendLines>\n\
          <line startValue='700000' istrendzone='1' valueOnRight='1' toolText='AYAN' endValue='900000' color='009933' displayvalue='Target' showontop = '1' thickness='5'/>\n\
       </trendLines>\n\
       <styles>\n\
          <definition>\n\
             <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
          </definition>\n\
          <application>\n\
             <apply toObject='Canvas' styles='CanvasAnim' />\n\
          </application>\n\
       </styles>\n\
    </chart>",

    ss3 = ss4 = ss5 = "<chart caption='Monthly' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
       <set label='Jan' value='420000' />\n\
       <set label='Feb' value='910000' />\n\
       <set label='Mar' value='' />\n\
       <set label='Apr' value='550000' />\n\
       <set label='May' value='810000' />\n\
    </chart>";

	ss6 = "<chart caption='Monthly' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>"
    for (var i = 0; i < 100; i += 1) {
		ss6 += "<set label='Label " + i + "' value='" + Math.round(Math.random() * 100) + "' />"; 
	}
	ss6 += "</chart>";

var ms1 = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
    <categories>\n\
        <category label='Jan' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <vline linePosition='1' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <category label='Apr' />\n\
    </categories>\n\
    <dataset seriesName='2006'>\n\
        <set value='27400' />\n\
        <set value='36700' />\n\
        <set value='-29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\
    </dataset>\n\
    <dataset seriesName='2005'>\n\
        <set value='10000'/>\n\
        <set value='-11500'/>\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
    </dataset>\n\
</chart>",

    ms2 = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
    <categories>\n\
        <category label='Jan' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <categorys label='Jul' />\n\
        <categorys label='Aug' />\n\
        <categorys label='Sep' />\n\
        <categorys label='Oct' />\n\
        <categorys label='Nov' />\n\
        <categorys label='Dec' />\n\
    </categories>\n\
    <dataset seriesName='2006'>\n\
        <set value='27400' />\n\
        <set value=''/>\n\
        <set value='25800' />\n\
        <set value='26800' />\n\
        <set value='29600' />\n\
        <set value='31800' />\n\
        <set value='' />\n\
        <set value='36700' />\n\
        <set value='29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\
    </dataset>\n\
    <dataset seriesName='2005'>\n\
        <set value='10000'/>\n\
        <set value='11500'/>\n\
        <set value='12333'/>\n\
        <set value=''/>\n\
        <set value='12333' />\n\
        <set value='9800' />\n\
        <set value='11800' />\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
    </dataset>\n\
</chart>",


    ms3 = ms4 = ms5 ="<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
    <categories>\n\
        <category label='Jan' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <categorys label='Jul' />\n\
        <categorys label='Aug' />\n\
        <categorys label='Sep' />\n\
        <categorys label='Oct' />\n\
        <categorys label='Nov' />\n\
        <categorys label='Dec' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
        <category label='Feb' />\n\
        <category label='Mar' />\n\
        <category label='Apr' />\n\
        <categorys label='May' />\n\
        <categorys label='Jun' />\n\
    </categories>\n\
    <dataset seriesName='2006'>\n\
        <set value='27400' />\n\
        <set value='12313'/>\n\
        <set value='25800' />\n\
        <set value='26800' />\n\
        <set value='29600' />\n\
        <set value='31800' />\n\
        <set value='23423' />\n\
        <set value='36700' />\n\
        <set value='29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\\n\
        <set value='12313'/>\n\
        <set value='25800' />\n\
        <set value='26800' />\n\
        <set value='29600' />\n\
        <set value='31800' />\n\
        <set value='23423' />\n\
        <set value='36700' />\n\
        <set value='29700' />\n\
        <set value='31900' />\n\
        <set value='34800' />\n\
        <set value='24800' />\n\\n\
    </dataset>\n\
    <dataset seriesName='2005'>\n\
        <set value='10000'/>\n\
        <set value='11500'/>\n\
        <set value='12333'/>\n\
        <set value='34534'/>\n\
        <set value='12333' />\n\
        <set value='9800' />\n\
        <set value='11800' />\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
        <set value='11500'/>\n\
        <set value='12333'/>\n\
        <set value='34534'/>\n\
        <set value='12333' />\n\
        <set value='9800' />\n\
        <set value='11800' />\n\
        <set value='19700' />\n\
        <set value='21700' />\n\
        <set value='21900' />\n\
        <set value='22900' />\n\
        <set value='20800' />\n\
    </dataset>\n\
</chart>";

var  HM1 = "<chart caption='Periodic Table' subCaption='Other Metals-Metalloids' \n\
xAxisName='Group' yAxisName='Period' canvasBgColor='FFFFFF' mapByCategory='1'>\n\
<colorRange>\n\
  <color code ='CCCCCC' label='Other metals'/>\n\
  <color code ='CCCC99' label='Metalloids'/>\n\
</colorRange>\n\
<dataset>\n\
  <set columnId='13' rowId='2' colorRangeLabel='Metalloids' displayValue='B'/>\n\
  <set columnId='13' rowId='3' colorRangeLabel='Other metals' displayValue='Al'/>\n\
  <set columnId='14' rowId='3' colorRangeLabel='Metalloids' displayValue='Si'/>\n\
  <set columnId='13' rowId='4' colorRangeLabel='Other metals' displayValue='Ga'/>\n\
  <set columnId='14' rowId='4' colorRangeLabel='Metalloids' displayValue='Ge'/>\n\
  <set columnId='15' rowId='4' colorRangeLabel='Metalloids' displayValue='As'/>\n\
  <set columnId='13' rowId='5' colorRangeLabel='Other metals' displayValue='In'/>\n\
  <set columnId='14' rowId='5' colorRangeLabel='Other metals' displayValue='Sn'/>\n\
  <set columnId='15' rowId='5' colorRangeLabel='Metalloids' displayValue='Sb'/>\n\
  <set columnId='16' rowId='5' colorRangeLabel='Metalloids' displayValue='Te'/>\n\
  <set columnId='13' rowId='6' colorRangeLabel='Other metals' displayValue='Tl'/>\n\
  <set columnId='14' rowId='6' colorRangeLabel='Other metals' displayValue='Pb'/>\n\
  <set columnId='15' rowId='6' colorRangeLabel='Other metals' displayValue='Bi'/>\n\
  <set columnId='16' rowId='6' colorRangeLabel='Metalloids' displayValue='Po'/>\n\
</dataset>\n\n\
</chart>",

	HM2 = "<chart Caption='Weekly Performance' subCaption='In percentage' xAxisName='Week Days' yAxisName='Companies'>\n\
    \n\
        <rows>\n\
                <row id='Google'/>\n\
                <row id='Yahoo'/>\n\
                <row id='Microsoft'/>\n\
                <row id='Symantec'/>\n\
                <row id='AOL'/>\n\
                \n\
        </rows>\n\
\n\
        <columns>\n\
\n\
                <column id='MON'/>\n\
                <column id='TUE'/>\n\
                <column id='WED'/>\n\\n\
                <column id='THU'/>\n\\n\\n\
                <column id='FRI'/>\n\
                \n\\n\
        </columns>\n\
        \n\
    <dataset>\n\
    \n\
     <set rowId='Yahoo' columnId='Mon' value='0'/>\n\
     <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
     <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
     <set rowId='Yahoo' columnId='Thu' value='63'/>\n\
     <set rowId='Yahoo' columnId='Fri' value='79'/>\n\\n\
         \n\
     <set rowId='Google' columnId='Mon' value='68'/>\n\
     <set rowId='Google' columnId='Tue' value='35'/>\n\
     <set rowId='Microsoft' columnId='Wed' value='31'/>\n\
     <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
     <set rowId='Google' columnId='Fri' value='55.98'/>\n\
         \n\
     <set rowId='Microsoft' columnId='Mon' value='98'/>\n\
     <set rowId='Microsoft' columnId='Tue' value='48'/>\n\
     <set rowId='Google' columnId='Wed' value='95'/>\n\
     <set rowId='Google' columnId='Thu' value='17'/>\n\
     <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
         \n\
     <set rowId='Symantec' columnId='Mon' value='89'/>\n\
     <set rowId='Symantec' columnId='Tue' value='68'/>\n\
     <set rowId='Symantec' columnId='Wed' value='90'/>\n\
     <set rowId='Symantec' columnId='Thu' value='19'/>\n\
     <set rowId='Symantec' columnId='Fri' value='69'/>\n\
     <set rowId='AOL' columnId='Mon' value='58'/>\n\
     <set rowId='AOL' columnId='Tue' value='27'/>\n\
     <set rowId='AOL' columnId='Wed' value='100'/>\n\
     <set rowId='AOL' columnId='Thu' value='70'/>\n\
     <set rowId='AOL' columnId='Fri' value='89'/>\n\
   </dataset>\n\
   <colorRange mapByPercent='1' gradient='1' minValue='0' code='57743A' startlabel='Very Bad' endLabel='Very Good' >\n\
        <color code ='8AA660'  maxValue='30' label='BAD'/>\n\
        <color code ='E5F2D0' maxValue='70' label='AVERAGE'/>\n\
        <color code ='2A5F55' maxValue='100' />\n\
   </colorRange>\n\
</chart>",
	HM3  = "<chart Caption='Weekly Attendance' \n\
subCaption='In percentage' xAxisLabel='Week Days' yAxisLabel='Companies' >\n\
<rows>\n\
   <row id='Google'/>\n\
   <row id='Yahoo'/>\n\
   <row id='Microsoft'/>\n\
</rows>\n\
\n\
<columns>\n\
   <column id='MON'/>\n\
   <column id='TUE'/>\n\
   <column id='WED'/>\n\\n\
   <column id='THU'/>\n\
   <column id='FRI'/>\n\
</columns>\n\
\n\
<dataset>\n\
\n\
   <set rowId='Yahoo' columnId='Mon' value='0'/>\n\
   <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
   <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
   <set rowId='Yahoo' columnId='Thu' value='3'/>\n\
   <set rowId='Yahoo' columnId='Fri' value='79'/>\n\
\n\
   <set rowId='Google' columnId='Mon' value='68'/>\n\
   <set rowId='Google' columnId='Tue' value='5'/>\n\\n\
   <set rowId='Microsoft' columnId='Wed' value='31'/>\n\
   <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
   <set rowId='Google' columnId='Fri' value='55.98'/>\n\
\n\
   <set rowId='Microsoft' columnId='Mon' value='98'/>\n\
   <set rowId='Microsoft' columnId='Tue' value='8'/>\n\
   <set rowId='Google' columnId='Wed' value='95'/>\n\
   <set rowId='Google' columnId='Thu' value='17'/>\n\
   <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
</dataset>\n\
 <colorRange mapbyPercent='1'>\n\
\n\
    <color code ='66ADD9' minValue='0' maxValue='30' label='BAD'/>\n\
    <color code ='F2CF63' minValue='30' maxValue='70' label='AVERAGE'/>\n\
    <color code ='D99036' minValue='70' maxValue='100' label='GOOD'/>\n\
\n\
   </colorRange>\n\
</chart>",
	HM4 ="<chart Caption='Weekly Attendance' \n\
subCaption='In percentage' xAxisLabel='Week Days' yAxisLabel='Companies' >\n\
<rows>\n\
   <row id='Google'/>\n\
   <row id='Yahoo'/>\n\
   <row id='Microsoft'/>\n\
</rows>\n\
\n\
<columns>\n\
   <column id='MON'/>\n\
   <column id='TUE'/>\n\
   <column id='WED'/>\n\\n\
   <column id='THU'/>\n\
   <column id='FRI'/>\n\
</columns>\n\
\n\
<dataset>\n\
\n\
   <set rowId='Yahoo' columnId='Mon' value='0'/>\n\
   <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
   <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
   <set rowId='Yahoo' columnId='Thu' value='3'/>\n\
   <set rowId='Yahoo' columnId='Fri' value='79'/>\n\
\n\
   <set rowId='Google' columnId='Mon' value='68'/>\n\
   <set rowId='Google' columnId='Tue' value='5'/>\n\\n\
   <set rowId='Microsoft' columnId='Wed' value='31'/>\n\
   <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
   <set rowId='Google' columnId='Fri' value='55.98'/>\n\
\n\
   <set rowId='Microsoft' columnId='Mon' value='98'/>\n\
   <set rowId='Microsoft' columnId='Tue' value='8'/>\n\
   <set rowId='Google' columnId='Wed' value='95'/>\n\
   <set rowId='Google' columnId='Thu' value='17'/>\n\
   <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
</dataset>\n\
 <colorRange mapbyPercent='0'>\n\
\n\
    <color code ='66ADD9' minValue='0' maxValue='30' label='BAD'/>\n\
    <color code ='F2CF63' minValue='30' maxValue='70' label='AVERAGE'/>\n\
    <color code ='D99036' minValue='70' maxValue='100' label='GOOD'/>\n\
\n\
   </colorRange>\n\
</chart>",

	HM5 = "<chart caption='Periodic Table' subCaption='Other Metals-Metalloids' \n\
xAxisName='Group' yAxisName='Period' canvasBgColor='FFFFFF' mapByCategory='1'>\n\
<colorRange>\n\
  <color code ='CCCCCC' label='Other metals'/>\n\
  <color code ='CCCC99' label='Metalloids'/>\n\
</colorRange>\n\
<dataset>\n\
  <set columnId='13' rowId='2' colorRangeLabel='Other metals' tlLabel='1' brLabel='Hydrogen' trLabel='1.00794' blLabel='3' displayValue='H' />\n\
  <set columnId='13' rowId='3' colorRangeLabel='Other metals' displayValue='Al'/>\n\
  <set columnId='14' rowId='3' colorRangeLabel='Metalloids' displayValue='Si'/>\n\
  <set columnId='13' rowId='4' colorRangeLabel='Other metals' displayValue='Ga'/>\n\
  <set columnId='14' rowId='4' colorRangeLabel='Metalloids' displayValue='Ge'/>\n\
  <set columnId='15' rowId='4' colorRangeLabel='Metalloids' displayValue='As'/>\n\
  <set columnId='13' rowId='5' colorRangeLabel='Other metals' displayValue='In'/>\n\
  <set columnId='14' rowId='5' colorRangeLabel='Other metals' displayValue='Sn'/>\n\
  <set columnId='15' rowId='5' colorRangeLabel='Metalloids' displayValue='Sb'/>\n\
  <set columnId='16' rowId='5' colorRangeLabel='Metalloids' displayValue='Te'/>\n\
  <set columnId='13' rowId='6' colorRangeLabel='Other metals' displayValue='Tl'/>\n\
  <set columnId='14' rowId='6' colorRangeLabel='Other metals' displayValue='Pb'/>\n\
  <set columnId='15' rowId='6' colorRangeLabel='Other metals' displayValue='Bi'/>\n\
  <set columnId='16' rowId='6' colorRangeLabel='Metalloids' displayValue='Po'/>\n\
</dataset>\n\n\
</chart>"

	HM6 = "<chart Caption='Weekly Attendance' \n\
subCaption='In percentage' xAxisLabel='Week Days' yAxisLabel='Companies' >\n\
<rows>\n\
   <row id='Google'/>\n\
   <row id='Yahoo'/>\n\
   <row id='Microsoft'/>\n\
</rows>\n\
\n\
<columns>\n\
   <column id='MON'/>\n\
   <column id='TUE'/>\n\
   <column id='WED'/>\n\\n\
   <column id='THU'/>\n\
   <column id='FRI'/>\n\
</columns>\n\
\n\
<dataset>\n\
\n\
   <set rowId='Yahoo' columnId='Mon' value='0'/>\n\
   <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
   <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
   <set rowId='Yahoo' columnId='Thu' value='3'/>\n\
   <set rowId='Yahoo' columnId='Fri' value='79'/>\n\
\n\
   <set rowId='Google' columnId='Mon' value='68'/>\n\
   <set rowId='Google' columnId='Tue' value='5'/>\n\\n\
   <set rowId='Microsoft' columnId='Wed' value='31'/>\n\
   <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
   <set rowId='Google' columnId='Fri' value='55.98'/>\n\
\n\
   <set rowId='Microsoft' columnId='Mon' value='98'/>\n\
   <set rowId='Microsoft' columnId='Tue' value='8'/>\n\
   <set rowId='Google' columnId='Wed' value='95'/>\n\
   <set rowId='Google' columnId='Thu' value='17'/>\n\
   <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
</dataset>\n\
 <colorRange mapbyPercent='0' gradient='0'>\n\
\n\
    <color code ='66ADD9' minValue='0' maxValue='30' label='BAD'/>\n\
    <color code ='F2CF63' minValue='30' maxValue='70' label='AVERAGE'/>\n\
    <color code ='D99036' minValue='70' maxValue='100' label='GOOD'/>\n\
\n\
   </colorRange>\n\
</chart>";


var linedashed1 ="<chart palette='3' caption='Monthly Sales Summary' subcaption='For the year 2006' xAxisName='Month' yAxisMinValue='15000' yAxisName='Sales' numberPrefix='$' showValues='0' >\n\
	<set label='Jan' value='17400' dashed='1'/>\n\
	<set label='Feb' value='19800' />\n\
	<set label='Mar' value='21800' dashed='1'/>\n\
	<set label='Apr' value='23800' />\n\
	<set label='May' value='29600' />\n\
	<set label='Jun' value='27600' />\n\
	<set label='Jul' value='31800' />\n\
	<set label='Aug' value='39700' />\n\
	<set label='Sep' value='32800' />\n\
	<set label='Oct' value='21900' />\n\
	<set label='Nov' value='32900' />\n\
	<set label='Dec' value='39800' />\n\
</chart>";

//var datas = [Data1, Data2, Data3, Data4, Data5, Data6, Data7, Data8, Data9, Data10, Data11];
var sampleDatas = {
    column2d: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ["100 data points", ss6], ss3, ss4, ss5, ["MS Data", ms1]],
    column3d: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    mscolumn2d: [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    mscolumn3d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    stackedcolumn2d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    stackedcolumn3d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    scrollcolumn2d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    scrollstackedcolumn2d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    pie2d:[["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    pie3d:[["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    doughnut2d: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    doughnut3d:[["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    bar2d: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ms1]],
    msbar2d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    msbar3d:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    line: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5,[ "DashedLine", linedashed1], ["MS Data", ms1]],
    msline:  [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    stackedarea2d: [sa1, sa2, sa3, sa4, ["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ["SS Data", ss1]],
    msarea2d: [["-ve Value/ZeroPlane Vline", ms1], ["Nulls", ms2], ms3, ms4, ms5, ["SS Data", ss1]],
    area2d: [["-ve Value/ZeroPlane Vline", ss1], ["Nulls", ss2], ss3, ss4, ss5, ["MS Data", ss1]],
    spline: [sp1, sp2, sp3, ss1, ss2, ms1],
    splinearea: [sp1, sp2, sp3, ms1, ms2, ss1],
    errorbar: [],
    errorline: [],
    errorscatter: [],
    logmscolumn: [],
    logmsline: [],
    logmsarea: [],
    boxandwhisker2d: [BW1, BW2, BW3, BW4, BW5, BW6, BW7, BW8, BW9, BW10, BW11],
    multiaxisline: [MAL1],
    heatmap : [HM1, HM2, HM3, HM4, HM5,["SS",ss1]],
    jdacustomcombichart: [["MS1", ms1], ["MS2", ms2], ["MS3", ms3],
            ["MS4", ms4], ["MS5", ms5], ["SS Data!", ss1]]
};



