var xml = [];

xml['Column2D'] = "<chart caption='Monthly' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
   <set label='Jan' value='420000' />\n\
   <set label='Feb' value='910000' />\n\
   <set label='Mar' value='720000' />\n\
   <set label='Apr' value='550000' />\n\
   <set label='May' value='810000' />\n\
   <set label='Jun' value='510000' />\n\
   <set label='Jul' value='680000' />\n\
   <set label='Aug' value='620000' />\n\
   <set label='Sep' value='610000' />\n\
   <set label='Oct' value='490000' />\n\
   <set label='Nov' value='530000' />\n\
   <set label='Dec' value='330000' />\n\
   <trendLines>\n\
      <line startValue='700000' istrendzone='1' valueOnRight='1' toolText='AYAN' endValue='900000' color='009933' displayvalue='Target' showontop = '1' thickness='5'/>\n\
   </trendLines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['Column3D'] = "<chart caption='Monthly Revenue' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
   <set label='Jan' value='420000' />\n\
   <set label='Feb' value='910000' />\n\
   <set label='Mar' value='720000' />\n\
   <set label='Apr' value='550000' />\n\
   <set label='May' value='810000' />\n\
   <set label='Jun' value='510000' />\n\
   <set label='Jul' value='680000' />\n\
   <set label='Aug' value='620000' />\n\
   <set label='Sep' value='610000' />\n\
   <set label='Oct' value='490000' />\n\
   <set label='Nov' value='530000' />\n\
   <set label='Dec' value='330000' />\n\
   <trendLines>\n\
      <line startValue='700000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
</chart>";

xml['Pareto2D'] = xml['Pareto3D'] = xml['Column3D'];

xml['Pie3D'] = "<chart caption='Company Revenue' showPercentageValues='1'>\n\
   <set label='Services' value='26' />\n\
   <set label='Hardware' value='32' />\n\
   <set label='Software' value='42' />\n\
</chart>";

xml['Pie2D'] = "<chart caption='Company Revenue' showPercentageValues='1'>\n\
   <set label='Services' value='26' />\n\
   <set label='Hardware' value='32' />\n\
   <set label='Software' value='42' />\n\
</chart>";

xml['Line'] = "<chart caption='Monthly Revenue' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
   <set label='Jan' value='420000' />\n\
   <set label='Feb' value='910000' />\n\
   <set label='Mar' value='720000' />\n\
   <set label='Apr' value='550000' />\n\
   <set label='May' value='810000' />\n\
   <set label='Jun' value='510000' />\n\
   <set label='Jul' value='680000' />\n\
   <set label='Aug' value='620000' />\n\
   <set label='Sep' value='610000' />\n\
   <set label='Oct' value='490000' />\n\
   <set label='Nov' value='530000' />\n\
   <set label='Dec' value='330000' />\n\
   <trendLines>\n\
      <line startValue='700000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['Bar2D'] = xml['Bar3D'] = "<chart caption='Monthly Revenue' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
   <set label='Jan' value='420000' />\n\
   <set label='Feb' value='910000' />\n\
   <set label='Mar' value='720000' />\n\
   <set label='Apr' value='550000' />\n\
   <set label='May' value='810000' />\n\
   <set label='Jun' value='510000' />\n\
   <set label='Jul' value='680000' />\n\
   <set label='Aug' value='620000' />\n\
   <set label='Sep' value='610000' />\n\
   <set label='Oct' value='490000' />\n\
   <set label='Nov' value='530000' />\n\
   <set label='Dec' value='330000' />\n\
   <trendLines>\n\
      <line startValue='700000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['Area2D'] = "<chart caption='Monthly Revenue' xAxisName='Month' yAxisName='Revenue' numberPrefix='$' showValues='1'>\n\
   <set label='Jan' value='420000' />\n\
   <set label='Feb' value='910000' />\n\
   <set label='Mar' value='720000' />\n\
   <set label='Apr' value='550000' />\n\
   <set label='May' value='810000' />\n\
   <set label='Jun' value='510000' />\n\
   <set label='Jul' value='680000' />\n\
   <set label='Aug' value='620000' />\n\
   <set label='Sep' value='610000' />\n\
   <set label='Oct' value='490000' />\n\
   <set label='Nov' value='530000' />\n\
   <set label='Dec' value='330000' />\n\
   <trendLines>\n\
      <line startValue='700000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['Doughnut2D'] = "<chart caption='Company Revenue' showPercentageValues='1'>\n\
   <set label='Services' value='26' />\n\
   <set label='Hardware' value='32' />\n\
   <set label='Software' value='42' />\n\
</chart>";

xml['Doughnut3D'] = "<chart caption='Company Revenue' showPercentageValues='1'>\n\
   <set label='Services' value='26' />\n\
   <set label='Hardware' value='32' />\n\
   <set label='Software' value='42' />\n\
</chart>";

xml['MSColumn2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <vline />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";


xml['MSColumn3D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
</chart>";

xml['MSLine'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['MSArea'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
   </styles>\n\
</chart>";
xml['DragBar2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset></chart>";

xml['MSBar2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart>";

xml['MSBar3D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
</chart> ";

xml['StackedColumn2D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";
xml['Marimekko'] = '<chart showValues="0" caption="Market Share Analysis" subCaption="2009" numberPrefix="$" xAxisName="Market Segment" yAxisName="Market Share" legendCaption="Manufacturer" >\n\
   <categories>\n\
     <category label="Desktop"/>\n\
     <category label="Laptop"/>\n\
     <category label="Notebook"/>\n\
   </categories>\n\
   <dataset seriesName="A">\n\
     <set value="335000"/>\n\
     <set value="225100"/>\n\
     <set value="164200"/>\n\
   </dataset>\n\
   <dataset seriesName="B">\n\
     <set value="215000"/>\n\
     <set value="198000"/>\n\
     <set value="120000"/>\n\
   </dataset>\n\
   <dataset seriesName="C">\n\
     <set value="298000"/>\n\
     <set value="109300"/>\n\
     <set value="153600"/>\n\
   </dataset>\n\
</chart>';

xml['StackedColumn3D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
</chart> ";

xml['StackedArea2D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";

xml['StackedBar2D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";

xml['StackedBar3D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
</chart> ";

xml['ScrollMSStackedColumn2D'] = xml['MSStackedColumn2D'] = xml['MSStackedColumn3D'] = "<chart caption='Break-up of Annual Revenue' subcaption='In Million $' xaxisname='Year' Yaxisname='Sales in M$' numdivlines='3' numberPrefix='$' numberSuffix='M' showSum='1'>\n\
\n\
   <categories font='Arial' fontSize='12' fontColor='000000'>\n\
      <category label='2001'/>\n\
      <category label='2002'/>\n\
      <category label='2003'/>\n\
      <category label='2004'/>\n\
      <category label='2005'/>\n\
   </categories>\n\
\n\
   <dataset>\n\
      <dataSet seriesName='Product A' color='AFD8F8' >\n\
         <set value='30' />\n\
         <set value='26' />\n\
         <set value='29' />\n\
         <set value='31' />\n\
         <set value='34' />\n\
      </dataSet>\n\
      <dataSet seriesName='Product B' color='F6BD0F' >\n\
         <set value='21' />\n\
         <set value='28' />\n\
         <set value='39' />\n\
         <set value='41' />\n\
         <set value='24' />\n\
      </dataSet>   \n\
   </dataset>\n\
   \n\
   <dataset>\n\
      <dataset seriesname='Service A' color='8BBA00' >\n\
         <set value='27' />\n\
         <set value='25' />\n\
         <set value='28' />\n\
         <set value='26' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service B' color='A66EDD' >\n\
         <set value='17' />\n\
         <set value='15' />\n\
         <set value='18' />\n\
         <set value='16' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service C' color='F984A1' >\n\
         <set value='12' />\n\
         <set value='17' />\n\
         <set value='16' />\n\
         <set value='15' />\n\
         <set value='12' />\n\
      </dataset>      \n\
   </dataset>\n\
\n\
</chart> ";

xml['MSCombi3D'] = xml['MSBarCombi3D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005' renderAs='Area'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <dataset seriesName='2004' renderAs='Line'>\n\
      <set value='7000'/>\n\
      <set value='10500'/>\n\
      <set value='9500'/>\n\
      <set value='10000'/>\n\
      <set value='9000' />\n\
      <set value='8800' />\n\
      <set value='9800' />\n\
      <set value='15700' />\n\
      <set value='16700' />\n\
      <set value='14900' />\n\
      <set value='12900' />\n\
      <set value='8800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='22000' color='91C728' displayValue='Target'/>\n\
   </trendlines>\n\
   <styles>\n\
      <definition>\n\
         <style name='bgAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='BACKGROUND' styles='bgAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['ScrollMSCombi2D'] = xml['MSCombi2D'] = xml['MSBarCombi2D'] ="<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005' renderAs='Area'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <dataset seriesName='2004' renderAs='Line'>\n\
      <set value='7000'/>\n\
      <set value='10500'/>\n\
      <set value='9500'/>\n\
      <set value='10000'/>\n\
      <set value='9000' />\n\
      <set value='8800' />\n\
      <set value='9800' />\n\
      <set value='15700' />\n\
      <set value='16700' />\n\
      <set value='14900' />\n\
      <set value='12900' />\n\
      <set value='8800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='22000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";


xml['MSColumnLine3D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
   <dataset seriesName='2005' renderAs='Line'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
</chart> ";


xml['MSCombiDY2D'] = xml['MSSplineDY'] = "<chart caption='Sales Volume' PYAxisName='Revenue' SYAxisName='Quantity' showvalues='0' numberPrefix='$'>\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
   <dataset seriesName='Revenue'>\n\
      <set value='170' />\n\
      <set value='610' />\n\
      <set value='142' />\n\
      <set value='135' />\n\
      <set value='214' />\n\
      <set value='121' />\n\
      <set value='113' />\n\
      <set value='156' />\n\
      <set value='212' />\n\
      <set value='900' />\n\
      <set value='132' />\n\
      <set value='101' />\n\
   </dataset>\n\
   <dataset seriesName='Quantity' parentYAxis='S'>\n\
      <set value='340' />\n\
      <set value='120' />\n\
      <set value='280' />\n\
      <set value='270' />\n\
      <set value='430' />\n\
      <set value='240' />\n\
      <set value='230' />\n\
      <set value='310' />\n\
      <set value='430' />\n\
      <set value='180' />\n\
      <set value='260' />\n\
      <set value='200' />\n\
   </dataset>\n\
   <trendLines>\n\
      <line startValue='2100000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
   <styles>\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>\n\
   </styles>\n\
</chart>";


xml['MSColumn3DLineDY'] = "<chart caption='Product Sales & Downloads' showValues='0' PYAxisName='Sales' SYAxisName='Total Downloads'>\n\
   <categories>\n\
      <category label='Jan'/>\n\
      <category label='Feb'/>\n\
      <category label='Mar'/>\n\
      <category label='Apr'/>\n\
      <category label='May'/>\n\
      <category label='Jun'/>\n\
      <category label='Jul'/>\n\
      <category label='Aug'/>\n\
      <category label='Sep'/>\n\
      <category label='Oct'/>\n\
      <category label='Nov'/>\n\
      <category label='Dec'/>\n\
   </categories>\n\
   <dataset seriesName='Product A Sales'>\n\
      <set value='230' />\n\
      <set value='245' />\n\
      <set value='250' />\n\
      <set value='245' />\n\
      <set value='350' />\n\
      <set value='330' />\n\
      <set value='360' />\n\
      <set value='340' />\n\
      <set value='230' />\n\
      <set value='220' />\n\
      <set value='200' />\n\
      <set value='190' />\n\
   </dataset>\n\
   <dataset seriesName='Product B Sales'>\n\
      <set value='130' />\n\
      <set value='145' />\n\
      <set value='50' />\n\
      <set value='115' />\n\
      <set value='190' />\n\
      <set value='130' />\n\
      <set value='160' />\n\
      <set value='140' />\n\
      <set value='130' />\n\
      <set value='120' />\n\
      <set value='140' />\n\
      <set value='80' />\n\
   </dataset>\n\
   <dataset seriesName='Total Downloads' parentYAxis='S'>\n\
      <set value='13000' />\n\
      <set value='14500' />\n\
      <set value='5000' />\n\
      <set value='11500' />\n\
      <set value='10000' />\n\
      <set value='13000' />\n\
      <set value='16000' />\n\
      <set value='14000' />\n\
      <set value='13000' />\n\
      <set value='12000' />\n\
      <set value='9000' />\n\
      <set value='10000' />\n\
   </dataset>\n\
   <trendlines>\n\
      <line startValue='300' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
</chart>";
xml['StackedColumn2DLine'] = xml['StackedColumn3DLine'] =
    xml['StackedBar2DLine'] = xml['StackedBar3DLine'] = "<chart palette='2' caption='Sales Chart' showvalues='0' numberPrefix='$' PYAxisName='Sales' SYAxisName='Quantity'>\n\
   <categories>\n\
      <category label='2001' />\n\
      <category label='2002' />\n\
      <category label='2003' />\n\
      <category label='2004' />\n\
      <category label='2005' />\n\
   </categories>\n\
   <dataset seriesName='Product A'>\n\
      <set value='25601' />\n\
      <set value='20148' />\n\
      <set value='17372' />\n\
      <set value='35407' />\n\
      <set value='38105' />\n\
   </dataset>\n\
   <dataset seriesName='Product B'>\n\
      <set value='57401' />\n\
      <set value='41941' />\n\
      <set value='45263' />\n\
      <set value='117320' />\n\
      <set value='114845' />\n\
   </dataset>\n\
   <dataset seriesName='Quantity' renderas='line' >\n\
      <set value='45000' />\n\
      <set value='44803' />\n\
      <set value='42830' />\n\
      <set value='77550' />\n\
      <set value='92630' />\n\
   </dataset>\n\
</chart>";

xml['StackedColumn3DLineDY'] = "<chart palette='2' caption='Sales Chart' showvalues='0' numberPrefix='$' PYAxisName='Sales' SYAxisName='Quantity'>\n\
   <categories>\n\
      <category label='2001' />\n\
      <category label='2002' />\n\
      <category label='2003' />\n\
      <category label='2004' />\n\
      <category label='2005' />\n\
   </categories>\n\
   <dataset seriesName='Product A'>\n\
      <set value='25601' />\n\
      <set value='20148' />\n\
      <set value='17372' />\n\
      <set value='35407' />\n\
      <set value='38105' />\n\
   </dataset>\n\
   <dataset seriesName='Product B'>\n\
      <set value='57401' />\n\
      <set value='41941' />\n\
      <set value='45263' />\n\
      <set value='117320' />\n\
      <set value='114845' />\n\
   </dataset>\n\
   <dataset seriesName='Quantity' parentYAxis='S' >\n\
      <set value='4500' />\n\
      <set value='4483' />\n\
      <set value='4283' />\n\
      <set value='7755' />\n\
      <set value='9263' />\n\
   </dataset>\n\
</chart>";


xml['MSStackedColumn3DLineDY'] = xml['MSStackedColumn2DLineDY'] = "<chart caption='Annual Revenue' subcaption='In Million $' xaxisname='Year' PYaxisname='Sales in M$' SYAxisName='Cost as % of Revenue' decimals='0' numberPrefix='$' numberSuffix='M' sNumberSuffix='%25' >\n\
   <categories>\n\
      <category label='2001'/>\n\
      <category label='2002'/>\n\
      <category label='2003'/>\n\
      <category label='2004'/>\n\
      <category label='2005'/>\n\
   </categories>\n\
   <dataset>\n\
      <dataset seriesName='Product A' color='AFD8F8' >\n\
         <set value='30' />\n\
         <set value='26' />\n\
         <set value='29' />\n\
         <set value='31' />\n\
         <set value='34' />\n\
      </dataset>\n\
      <dataset seriesName='Product B' color='F6BD0F' >\n\
         <set value='21' />\n\
         <set value='28' />\n\
         <set value='39' />\n\
         <set value='41' />\n\
         <set value='24' />\n\
      </dataset>\n\
   </dataset>\n\
   <dataset>\n\
      <dataset seriesname='Service A' color='8BBA00' >\n\
         <set value='27' />\n\
         <set value='25' />\n\
         <set value='28' />\n\
         <set value='26' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service B' color='A66EDD' >\n\
         <set value='17' />\n\
         <set value='15' />\n\
         <set value='18' />\n\
         <set value='16' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service C' color='F984A1' >\n\
         <set value='12' />\n\
         <set value='17' />\n\
         <set value='16' />\n\
         <set value='15' />\n\
         <set value='12' />\n\
      </dataset>\n\
   </dataset>\n\
   <lineset seriesname='Cost as % of Revenue' lineThickness='4' >\n\
      <set value='57' />\n\
      <set value='68' />\n\
      <set value='79' />\n\
      <set value='73' />\n\
      <set value='80' />\n\
   </lineset>\n\
</chart>";

xml['Scatter'] = xml['ZoomScatter'] = "<chart palette='2' caption='Server Performance' yAxisName='Response Time (sec)' xAxisName='Server Load (TPS)' xAxisMaxValue='100' xAxisMinValue='20' yAxisMaxValue='7'>\n\
\n\
   <categories verticalLineColor='666666' verticalLineThickness='1'>\n\
      <category label='20' x='20' showVerticalLine='1'/>\n\
      <category label='30' x='30' showVerticalLine='1'/>\n\
      <category label='40' x='40' showVerticalLine='1'/>\n\
      <category label='50' x='50' showVerticalLine='1'/>\n\
      <category label='60' x='60' showVerticalLine='1'/>\n\
      <category label='70' x='70' showVerticalLine='1'/>\n\
      <category label='80' x='80' showVerticalLine='1'/>\n\
      <category label='90' x='90' showVerticalLine='1'/>\n\
      <category label='100' x='100' showVerticalLine='0'/>\n\
   </categories>\n\
\n\
   <dataset drawLine='1' seriesName='Server 1' color='009900' anchorSides='3' anchorRadius='4' anchorBgColor='D5FFD5' anchorBorderColor='009900'>\n\
      <set y='2.4' x='21' />\n\
      <set y='3.5' x='32' />\n\
      <set y='2.5' x='43' />\n\
      <set y='4.1' x='48' />\n\
      <set y='3.5' x='50' />\n\
      <set y='4.6' x='56' />\n\
      <set y='4.8' x='59' />\n\
      <set y='4.9' x='73' />\n\
      <set y='4.6' x='89' />\n\
      <set y='4.2' x='93' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Server 2' color='0000FF' anchorSides='4' anchorRadius='4' anchorBgColor='C6C6FF' anchorBorderColor='0000FF'>\n\
      <set y='1.4' x='23'/>\n\
      <set y='1.5' x='29'/>\n\
      <set y='1.5' x='33'/>\n\
      <set y='1.1' x='41'/>\n\
      <set y='1.5' x='47'/>\n\
      <set y='1.6' x='49'/>\n\
      <set y='1.8' x='51'/>\n\
      <set y='1.9' x='53'/>\n\
      <set y='1.6' x='57'/>\n\
      <set y='1.2' x='58'/>\n\
      <set y='1.9' x='61'/>\n\
      <set y='1.1' x='63'/>\n\
      <set y='1.9' x='64'/>\n\
      <set y='1.7' x='71'/>\n\
      <set y='1.1' x='77'/>\n\
      <set y='1.3' x='79'/>\n\
      <set y='1.7' x='83'/>\n\
      <set y='1.8' x='89'/>\n\
      <set y='1.9' x='91'/>\n\
      <set y='1.0' x='93'/>\n\
   </dataset>\n\
\n\
   <vTrendlines>\n\
      <line startValue='20' endValue='65' alpha='5' color='00FF00' />\n\
      <line startValue='65' endValue='75' alpha='15' color='FFFF00' />\n\
      <line startValue='75' endValue='100' alpha='15' color='FF0000' />\n\
   </vTrendlines>\n\
\n\
   <trendlines>\n\
      <line startValue='5.2' displayValue='Check' lineThickness='2' color='FF0000' valueOnRight='1' dashed='1' dashGap='5'/>\n\
   </trendlines>\n\
\n\
</chart>\n\
";

xml['Bubble'] = "<chart palette='3' numberPrefix='$' is3D='1' xAxisMaxValue='100' showPlotBorder='0' xAxisName='Stickiness' yAxisName='Cost Per Service' chartRightMargin='30'>\n\
\n\
   <categories>\n\
      <category label='0%' x='0' />\n\
      <category label='20%' x='20' showVerticalLine='1'/>\n\
      <category label='40%' x='40' showVerticalLine='1'/>\n\
      <category label='60%' x='60' showVerticalLine='1'/>\n\
      <category label='80%' x='80' showVerticalLine='1'/>\n\
      <category label='100%' x='100' showVerticalLine='1'/>\n\
   </categories>\n\
\n\
   <dataSet seriesName='Bubbles'>\n\
      <set x='30' y='.3' z='116' name='Traders'/>\n\
      <set x='3' y='3.5' z='99' name='Farmers'/>\n\
      <set x='8' y='2.1' z='33' name='Individuals'/>\n\
      <set x='62' y='2.5' z='72' name='Medium Business Houses'/>\n\
      <set x='78' y='2.3' z='55' name='Corporate Group A'/>\n\
      <set x='75' y='1.4' z='58' name='Corporate Group C'/>\n\
      <set x='97' y='3.7' z='80' name='HNW Individuals'/>\n\
      <set x='50' y='4.5' z='105' name='Small Business Houses'/>\n\
   </dataSet>\n\
\n\
   <trendlines>\n\
      <line startValue='2.5' isTrendZone='0' displayValue='Median Cost' color='0372AB'/>\n\
   </trendlines>\n\
\n\
   <vTrendlines>\n\
      <line startValue='0' endValue='60' isTrendZone='1' displayValue='Potential Wins' color='663333' alpha='10'/>\n\
      <line startValue='60' endValue='100' isTrendZone='1' displayValue='Cash Cows' color='990099' alpha='5'/>\n\
   </vTrendlines>\n\
\n\
</chart>\n\
";

xml['ScrollColumn2D'] = xml['ScrollColumn3D'] = xml['ScrollBar2D'] =
    xml['ScrollBar3D']= "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' useRoundEdges='1'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";

xml['ScrollLine2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' numVisiblePlot='6'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";

xml['ScrollArea2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' numVisiblePlot='6'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart> ";

xml['ScrollStackedColumn2D'] = "<chart caption='Company Revenue' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' useRoundEdges='1'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='Product A'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Product B'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='42000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
</chart> ";

xml['ScrollCombi2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' numVisiblePlot='8'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005' renderAs='Area' showPlotBorder='0'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2004' renderAs='Line' lineThickness='3'>\n\
      <set value='7000'/>\n\
      <set value='10500'/>\n\
      <set value='9500'/>\n\
      <set value='10000'/>\n\
      <set value='9000' />\n\
      <set value='8800' />\n\
      <set value='9800' />\n\
      <set value='15700' />\n\
      <set value='16700' />\n\
      <set value='14900' />\n\
      <set value='12900' />\n\
      <set value='8800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='22000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>   \n\
\n\
</chart> ";

xml['ScrollCombiDY2D'] = "<chart caption='Sales Volume' PYAxisName='Revenue' SYAxisName='Quantity' showvalues='0' numberPrefix='$' numVisiblePlot='8' useRoundEdges='1' palette='4'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
<dataset seriesName='Quantity' parentYAxis='S'>\n\
      <set value='340' />\n\
      <set value='120' />\n\
      <set value='280' />\n\
      <set value='270' />\n\
      <set value='430' />\n\
      <set value='240' />\n\
      <set value='230' />\n\
      <set value='310' />\n\
      <set value='430' />\n\
      <set value='180' />\n\
      <set value='260' />\n\
      <set value='200' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='Revenue'>\n\
      <set value='1700000' />\n\
      <set value='610000' />\n\
      <set value='1420000' />\n\
      <set value='1350000' />\n\
      <set value='2140000' />\n\
      <set value='1210000' />\n\
      <set value='1130000' />\n\
      <set value='1560000' />\n\
      <set value='2120000' />\n\
      <set value='900000' />\n\
      <set value='1320000' />\n\
      <set value='1010000' />\n\
   </dataset>\n\
\n\
\n\
   <trendLines>\n\
      <line startValue='2100000' color='009933' displayvalue='Target' />\n\
   </trendLines>\n\
\n\
</chart> ";

xml['ZoomLine'] = "<chart compactDataMode='1' dataSeparator='|'>\n\
<categories>Label 1|Label 2|Label 3|Label 4|Label 5|Label 6|Label 7|Label 8|Label 9|Label 10|Label 11|Label 12|Label 13|Label 14|Label 15|Label 16|Label 17|Label 18|Label 19|Label 20|Label 21|Label 22|Label 23|Label 24|Label 25|Label 26|Label 27|Label 28|Label 29|Label 30|Label 31|Label 32|Label 33|Label 34|Label 35|Label 36|Label 37|Label 38|Label 39|Label 40|Label 41|Label 42|Label 43|Label 44|Label 45|Label 46|Label 47|Label 48|Label 49|Label 50</categories>\n\
<dataset seriesName='Series 1' color='AFD8F8'>182|236|17|406|676|346|109|644|421|484|692|246|856|125|399|418|58|552|236|511|767|926|420|735|584|352|952|320|170|536|487|675|329|411|886|226|17|642|477|50|274|542|387|471|878|1|991|919|298|914</dataset>\n\
<dataset seriesName='Series 2' color='F6BD0F'>294|170|450|120|844|703|599|579|771|318|57|841|740|440|327|92|683|389|849|521|675|980|112|972|434|493|653|314|676|350|464|286|427|5|579|809|338|819|734|821|686|58|587|491|400|185|505|460|412|874</dataset></chart>";

xml['DateAxis'] ="<chart caption='Sales Volume'  subCaption='In Indian Rupee' PYAxisName='Revenue' SYAxisName='Quantity' showvalues='0' numberPrefix='$' xAxisName='Time' inputDateFormat='M/d/yyyy' outputDateFormat='yyyy-MM-dd' tooltipDateFormat='dd~yyyy~MM' categoryDateFormat='dd/MM/yyyy'>\n\
	<categories>\n\
		<category label='2/14/2012'/>\n\
		<category label='2/15/2012'/>\n\
		<category label='2/16/2012'/>\n\
		<category label='2/17/2012'/>\n\
		<category label='2/18/2012'/>\n\
		<category label='2/19/2012'/>\n\
		<category label='2/20/2012'/>\n\
		<category label='2/21/2012'/>\n\
		<category label='2/22/2012'/>\n\
		<category label='2/23/2012'/>\n\
		<category label='2/24/2012'/>\n\
		<category label='2/25/2012'/>\n\
	</categories>\n\
	<dataset seriesName='Revenue' renderAs='line'>\n\
		<set value='827' time='2/14/1996'/>\n\
		<set value='750' time='1/9/1992'/>\n\
		<set value='523' time='5/27/1993'/>\n\
		<set value='724' time='6/20/1995'/>\n\
		<set value='287' time='4/30/1992'/>\n\
		<set value='851' time='3/14/1991'/>\n\
		<set value='244' time='10/19/2005'/>\n\
		<set value='537' time='3/19/2002'/>\n\
		<set value='176' time='8/18/2009'/>\n\
		<set value='673' time='11/19/1994'/>\n\
		<set value='599' time='10/10/2000'/>\n\
		<set value='638' time='12/5/1994'/>\n\
	</dataset>\n\
	<dataset seriesName='Quantity'  renderAs='line' parentYAxis='S'>\n\
		<set value='129' time='12/20/2001'/>\n\
		<set value='319' time='10/19/2006'/>\n\
		<set value='801' time='8/16/2012'/>\n\
		<set value='359' time='10/1/2009'/>\n\
		<set value='42' time='10/13/1997'/>\n\
		<set value='799' time='4/25/2004'/>\n\
		<set value='465' time='1/30/2006'/>\n\
		<set value='886' time='1/12/2012'/>\n\
		<set value='700' time='10/11/2009'/>\n\
		<set value='377' time='4/17/1996'/>\n\
		<set value='649' time='4/26/1994'/>\n\
		<set value='766' time='9/8/2006'/>\n\
	</dataset>\n\
\n\
\n\
	<trendLines>\n\
		<line startValue='2100000' color='009933' displayvalue='Target' />\n\
	</trendLines>\n\
	<styles>\n\
		<definition>\n\
			<style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
		</definition>\n\
		<application>\n\
			<apply toObject='Canvas' styles='CanvasAnim' />\n\
		</application>\n\
	</styles>\n\
</chart>";

xml['Spline'] = xml['Column2D'];
xml['SplineArea'] = xml['Column2D'];
xml['MSSpline'] = xml['MSLine'];
xml['MSSplineArea'] = xml['MSLine'];
xml['InverseMSLine'] = xml['MSLine'];
xml['InverseMSColumn2D'] = xml['MSColumn2D'];
xml['InverseMSColumn3D'] = xml['MSColumn2D'];
xml['InverseMSBar'] = xml['MSColumn2D'];
xml['InverseMSBar3d'] = xml['MSColumn2D'];
xml['InverseMSArea'] = xml['MSArea'];
xml['SelectScatter'] = xml['Scatter'];
xml['DragColumn2D'] = xml['MSColumn2D'];
xml['DragLine'] = xml['MSLine'];
xml['DragArea'] = xml['MSArea'];

xml['MultiAxisLine'] = "<chart caption='Power Generator' xAxisName='Time' showValues='0' divLineAlpha='100' numVDivLines='4' vDivLineAlpha='0' showAlternateVGridColor='1' alternateVGridAlpha='5'>\n\
   <categories>\n\
      <category label='00:00s' />\n\
      <category label='00:04s' />\n\
      <category label='00:08s' />\n\
      <category label='00:12s' />\n\
      <category label='00:16s' />\n\
      <category label='00:20s' />\n\
   </categories>\n\
   <axis title='Power' titlePos='left' tickWidth='10' divlineisdashed='1'>\n\
      <dataset seriesName='Power [W]' >\n\
         <set value='6' />\n\
         <set value='26' />\n\
         <set value='16' />\n\
         <set value='27' />\n\
         <set value='28' />\n\
         <set value='33' />\n\
      </dataset>\n\
   </axis>\n\
   <axis title='Temp.' titlePos='left' numDivLines='14' tickWidth='10' divlineisdashed='1' >\n\
      <dataset seriesName='Temperature'>\n\
         <set value='296' />\n\
         <set value='311' />\n\
         <set value='336' />\n\
         <set value='371' />\n\
         <set value='389' />\n\
         <set value='393' />\n\
      </dataset>\n\
   </axis>\n\
   <axis title='Speed[RPM]' titlepos='RIGHT' axisOnLeft='0' numDivLines='10' tickWidth='10' divlineisdashed='1'>\n\
      <dataset seriesName='Speed'>\n\
         <set value='1' />\n\
         <set value='11' />\n\
         <set value='36' />\n\
         <set value='49' />\n\
         <set value='68' />\n\
         <set value='88' />\n\
      </dataset>\n\
   </axis>\n\
</chart> ";
xml['MultiLevelPie'] = "<chart palette='2' piefillAlpha='34' pieBorderThickness='3' hoverFillColor='FDCEDA' pieBorderColor='FFFFFF' baseFontSize='9' useHoverColor='1' caption='Organization Chart' >\n\
   <category label='CEO' fillColor='DBFF6C' link='n-Details.asp?CEO'>\n\
      <category label='CTO' fillColor='DBFF6C'>\n\
         <category label='Proj. Manager' >\n\
            <category label='Design' />\n\
            <category label='Coding' />\n\
            <category label='Validate' />\n\
         </category>\n\
         <category label='Q & A Manager' >\n\
            <category label='Testing Team' />\n\
         </category>\n\
         <category label='Architect' >\n\
            <category label='Study' />\n\
            <category label='Design' />\n\
         </category>\n\
      </category>\n\
   <category label='CFO' fillColor='FBE299' >\n\
      <category label='Payables' hoverText='Accounts Payable Team'>\n\
         <category label='Salary' hoverText='Team for salary accounts maintenance and upkeep' />\n\
         <category label='Purchase' />\n\
         <category label='Other' hoverText='Other Payments' />\n\
      </category>\n\
      <category label='Receivables' hoverText='Accounts Receivables Team'>\n\
         <category label='SW1' hoverText='Online Software Receipts Accounts' />\n\
         <category label='SW2' hoverText='Physical Software Receipts Accounts' />\n\
         <category label='SER1' hoverText='Services Collection' />\n\
         <category label='SER2' hoverText='Services Collection (Physical)' />\n\
         <category label='OTR' hoverText='Subscription and Other Collections' />\n\
      </category>\n\
   </category>\n\
   <category label='CIO' fillColor='DAEDFC' link='Details.asp?CIO'>\n\
      <category label='PR' hoverText='PR Team'>\n\
         <category label='Packaging' hoverText='Packaging Staff' />\n\
         <category label='Inv Rel.' hoverText='Investor Relations Upkeep' />\n\
         <category label='Marketing' hoverText='Marketing & Sales' />\n\
      </category>\n\
      <category label='HR' hoverText='HR Team'>\n\
         <category label='Selection' hoverText='Selection of Candidates' />\n\
         <category label='Deploying' hoverText='Deploying at required site' />\n\
      </category>\n\
   </category>\n\
</category>\n\
<styles>\n\
   <definition>\n\
      <style name='myHTMLFont' type='font' isHTML='1' />\n\
   </definition>\n\
   <application>\n\
      <apply toObject='TOOLTIP' styles='myHTMLFont' />\n\
   </application>\n\
</styles>\n\
</chart>";

xml['Waterfall2D'] = "<chart caption='Sales By Prduct' subcaption='For the year 2006' xAxisName='Product' yAxisName='Sales' numberPrefix='$' showValues='1' connectorDashed='1'>\n\
   <set label='Bi v2' value='420000' />\n\
   <set label='Real v3.2' value='810000' />\n\
   <set label='Business Division Sales' isSum='1' />\n\
   <set label='FusionCharts' value='910000' />\n\
   <set label='IS v2' value='720000' />\n\
   <set label='PS v3' value='550000' />\n\
</chart>";

xml['MSStepLine'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$' drawVerticalJoints='1' useForwardSteps='1' lineDashed='0' numDivLines='1'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
</chart>\n\
";
xml['ErrorBar2D'] = xml['HorizontalErrorBar2D'] = "<chart palette='2' xaxisname='Appliance' yaxisname='Life Span (yr)' caption='Life Span Chart of Appliances' subcaption='(With Error Values)' showValues='0' halfErrorBar='0' errorBarColor='666666'>\n\
\n\
   <categories>\n\
      <category label='Central AC' />\n\
      <category label='Window AC' />\n\
      <category label='Pump' />\n\
      <category label='Tea Maker' />\n\
      <category label='Washer' />\n\
   </categories>\n\
\n\
   <dataset seriesName='At Home' >\n\
      <set value='14' errorValue='2' />\n\
      <set value='9' errorValue='1.5' />\n\
      <set value='15' errorValue='3' />\n\
      <set value='6' errorValue='1.8' />\n\
      <set value='10' errorValue='1.2' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='In Office' >\n\
      <set value='11' errorValue='1' />\n\
      <set value='6' errorValue='0.5' />\n\
      <set value='12' errorValue='2' />\n\
      <set value='4' errorValue='0.8' />\n\
      <set value='7' errorValue='1' />\n\
   </dataset>\n\
   </chart>";
xml['ErrorScatter'] = "<chart palette='1' caption='Please select the required points from chart below' yAxisName='Value' xAxisName='Load (TPS)' showLegend='1' showNames='1' xAxisMaxValue='100' xAxisMinValue='0' submitDataAsXML='1' errorBarColor='000000' errorBarThickness='3' errorBarWidth='20' valuePadding='40' useHorizontalErrorBar='0' drawQuadrant='0' halfErrorBar='0' >\n\
\n\
<categories verticalLineColor='AA6666' verticalLineThickness='1'>\n\
   <category label='20' x='15' showVerticalLine='1'/>\n\
   <category label='30' x='25' showVerticalLine='1'/>\n\
   <category label='40' x='35' showVerticalLine='1'/>\n\
   <category label='50' x='45' showVerticalLine='1'/>\n\
   <category label='60' x='55' showVerticalLine='1'/>\n\
</categories>\n\
\n\
<dataSet id='DS1' seriesName='Dataset 1' Color='ff5904' plotBorderThickness='0' showPlotBorder='1' useHorizontalErrorBar='1' drawLine='0'>\n\
   <set id='S1' x='20' y='20' errorValue='15' useHorizontalErrorBar='1'/>\n\
   <set id='S2' x='30' y='70' errorValue='50' useHorizontalErrorBar=''/>\n\
   <set id='S3' x='40' y='40' errorValue='21' useHorizontalErrorBar='0'/>\n\
   <set id='S4' x='50' y='50' errorValue='10' useHorizontalErrorBar=''/>\n\
   <set id='S5' x='60' y='60' errorValue='11' useHorizontalErrorBar='0'/>\n\
</dataSet>\n\
\n\
</chart> ";
xml['ErrorLine'] = "<chart palette='2' xaxisname='Appliance' yaxisname='Life Span (yr)' caption='Life Span Chart of Appliances' subcaption='(With Error Values)' showValues='0' halfErrorBar='0' errorBarColor='ff0000' errorBarThickness='2' errorBarWidth='20' canvasPadding=''>\n\
\n\
   <categories>\n\
      <category label='AC - Central' />\n\
      <category label='AC - Window' />\n\
      <category label='Heat Pump' />\n\
      <category label='Coffee Machine' />\n\
      <category label='Dish Washer' />\n\
   </categories>\n\
\n\
   <dataset seriesName='At Home'>\n\
      <set value='50' errorValue='10' />\n\
      <set value='-100' errorValue='12' />\n\
      <set value='120' errorValue='19' />\n\
      <set value='90' errorValue='11' />\n\
      <set value='-98' errorValue='15' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='In Office' >\n\
      <set value='-11' errorValue='15' />\n\
      <set value='60' errorValue='13' />\n\
      <set value='-12' errorValue='16' />\n\
      <set value='40' errorValue='15.8' />\n\
      <set value='70' errorValue='17' />\n\
   </dataset>\n\
</chart>";
xml["LogMSLine"] = xml["LogStackedColumn2D"] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
</chart>";
xml['Radar'] = "<chart caption='Radar Chart'>\n\
   <categories>\n\
      <category label='Index 1' />\n\
      <category label='Index 2' />\n\
      <category label='Index 3' />\n\
      <category label='Index 4' />\n\
      <category label='Index 5' />\n\
      <category label='Index 6' />\n\
      <category label='Index 7' />\n\
      <category label='Index 8' />\n\
      <category label='Index 9' />\n\
      <category label='Index 10' />\n\
      <category label='Index 11' />\n\
   </categories>\n\
   <dataset seriesName='Series 1'>\n\
      <set value='9' />\n\
      <set value='9' />\n\
      <set value='9' />\n\
      <set value='7' />\n\
      <set value='8' />\n\
      <set value='8' />\n\
      <set value='9' />\n\
      <set value='9' />\n\
      <set value='9' />\n\
      <set value='7' />\n\
      <set value='8' />\n\
   </dataset>\n\
   <dataset seriesName='Series 2'>\n\
      <set value='5' />\n\
      <set value='3' />\n\
      <set value='2' />\n\
      <set value='4' />\n\
      <set value='5' />\n\
      <set value='9' />\n\
      <set value='5' />\n\
      <set value='3' />\n\
      <set value='2' />\n\
      <set value='4' />\n\
      <set value='5' />\n\
   </dataset>\n\
</chart>";
xml['DragNode'] = "<chart palette='2' xAxisMinValue='0' xAxisMaxValue='100' yAxisMinValue='0' yAxisMaxValue='100' is3D='1' showFormBtn='1' viewMode='0'>\n\
   <dataset>\n\
      <set x='12' y='79' width='70' height='56' name='Node 1 with a long label' color='FE3233' id='1'/>\n\
      <set x='59' y='15' width='40' height='56' name='Node 2' color='FE9191' id='2'/>\n\
      <set x='88' y='75' radius='37' shape='circle' name='Node 3' color='62D0FE' id='3'/>\n\
      <set x='33' y='35' radius='37' shape='circle' name='Node 4' color='FE8181' id='4'/>\n\
      <set x='40' y='85' width='45' height='67' name='Node 5' color='FE7272' BorderColor='ff5904' id='5' />\n\
      <set x='69' y='85' width='45' height='67' name='Node 6' color='72D4FE' id='6' />\n\
      <set x='87' y='45' radius='47' shape='polygon' numSides='3' name='Node 7' color='FE5151' id='7' />\n\
      <set x='65' y='60' radius='47' shape='polygon' numSides='5' name='Node 8' color='FE9191' id='8' />\n\
      <set x='12' y='20' radius='37' shape='circle' name='Node 9' color='33C1FE' id='9' />\n\
      <set x='80' y='12' radius='47' shape='polygon' numSides='4' name='Node 10' color='33C1FE' id='10' />\n\
   </dataset>\n\
   <connectors color='FF0000' stdThickness='5'>\n\
      <connector strength='0.96' label='link label' from='1' to='9' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.8' label='link12' from='1' to='5' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.8' label='link2' from='1' to='8' color='BBBB00' />\n\
      <connector strength='0.3' label='link3' from='1' to='4' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.4' label='link4' from='2' to='4' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.6' label='link5' from='4' to='2' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.33' label='link1' from='2' to='8' color='BBBB00'/>\n\
      <connector strength='0.66' label='link7' from='8' to='3' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.7' label='link6' from='4' to='5' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.6' from='4' to='8' color='FF5904' arrowAtStart='1' arrowAtEnd='1'/>\n\
      <connector strength='0.6' label='link9' from='5' to='8' color='BBBB00' />\n\
      <connector strength='0.5' label='link10' from='7' to='8' color='BBBB00' arrowAtStart='1' arrowAtEnd='1' />\n\
      <connector strength='0.3' label='link11' from='7' to='10' color='BBBB00'/>\n\
   </connectors>\n\
</chart>";
xml['LogMSColumn2D'] = "<chart caption='Business Results 2005 v 2006' xAxisName='Month' yAxisName='Revenue' showValues='0' numberPrefix='$'>\n\
\n\
   <categories>\n\
      <category label='Jan' />\n\
      <category label='Feb' />\n\
      <category label='Mar' />\n\
      <category label='Apr' />\n\
      <category label='May' />\n\
      <category label='Jun' />\n\
      <category label='Jul' />\n\
      <category label='Aug' />\n\
      <category label='Sep' />\n\
      <category label='Oct' />\n\
      <category label='Nov' />\n\
      <category label='Dec' />\n\
   </categories>\n\
\n\
   <dataset seriesName='2006'>\n\
      <set value='27400' />\n\
      <set value='29800'/>\n\
      <set value='25800' />\n\
      <set value='26800' />\n\
      <set value='29600' />\n\
      <set value='32600' />\n\
      <set value='31800' />\n\
      <set value='36700' />\n\
      <set value='29700' />\n\
      <set value='31900' />\n\
      <set value='34800' />\n\
      <set value='24800' />\n\
   </dataset>\n\
\n\
   <dataset seriesName='2005'>\n\
      <set value='10000'/>\n\
      <set value='11500'/>\n\
      <set value='12500'/>\n\
      <set value='15000'/>\n\
      <set value='11000' />\n\
      <set value='9800' />\n\
      <set value='11800' />\n\
      <set value='19700' />\n\
      <set value='21700' />\n\
      <set value='21900' />\n\
      <set value='22900' />\n\
      <set value='20800' />\n\
   </dataset>\n\
\n\
   <trendlines>\n\
      <line startValue='26000' color='91C728' displayValue='Target' showOnTop='1'/>\n\
   </trendlines>\n\
\n\
   <styles>\n\
\n\
      <definition>\n\
         <style name='CanvasAnim' type='animation' param='_xScale' start='0' duration='1' />\n\
      </definition>\n\
\n\
      <application>\n\
         <apply toObject='Canvas' styles='CanvasAnim' />\n\
      </application>   \n\
\n\
   </styles>\n\
\n\
</chart>";


xml['CandleStick'] = '<chart numPDivLines="5" caption="XYZ - 3 Months" numberPrefix="$" bearBorderColor="E33C3C" bearFillColor="E33C3C" bullBorderColor="1F3165" PYAxisName="Price" VYAxisName="Volume (In Millions)" volumeHeightPercent="20">\n\
   <categories>\n\
      <category label="2006" x="1" showLine="1" /> \n\
   </categories>\n\
   <dataset>\n\
      <set open="24.6" high="25.24" low="24.58" close="25.19" x="1" volume="17856350" /> \n\
      <set open="24.36" high="24.58" low="24.18" close="24.41" x="2" volume="3599252" /> \n\
      <set open="24.63" high="24.66" low="24.11" close="24.95" x="3" volume="74685351" /> \n\
      <set open="24.53" high="24.84" low="24.01" close="24.95" x="4" volume="49236987" /> \n\
      <set open="24.84" high="24.94" low="24.56" close="24.93" x="5" volume="18247006" /> \n\
      <set open="24.96" high="25.03" low="24.58" close="24.89" x="6" volume="67419690" /> \n\
      <set open="25.25" high="25.46" low="25.11" close="25.13" x="7" volume="95517555" /> \n\
      <set open="25.27" high="25.37" low="25.0999" close="25.18" x="8" volume="83656552" /> \n\
      <set open="25.33" high="25.43" low="25.06" close="25.16" x="9" volume="42177624" /> \n\
      <set open="25.38" high="25.51" low="25.23" close="25.38" x="10" volume="40668662" /> \n\
      <set open="25.2" high="25.78" low="25.07" close="25.09" x="11" volume="78602232" />\n\
      <set open="25.66" high="25.8" low="25.35" close="25.37" x="12" volume="10338104" /> \n\
      <set open="25.77" high="25.97" low="25.54" close="25.72" x="13" volume="38067037" /> \n\
      <set open="26.31" high="26.35" low="25.81" close="25.83" x="14" volume="52104215" /> \n\
      <set open="26.23" high="26.6" low="26.2" close="26.35" x="15" volume="46274157" />\n\
      </dataset>\n\
   <trendset name="Simple Moving Average" color="0099FF" thickness="0.5" alpha="100" includeInLegend="1">\n\
      <set x="1" value="24.6" /> \n\
      <set x="2" value="24.69" /> \n\
      <set x="3" value="24.89" /> \n\
      <set x="4" value="24.92" /> \n\
      <set x="5" value="25.2" /> \n\
      <set x="6" value="25.1" />\n\
      <set x="7" value="25.17" />\n\
      <set x="8" value="25.2" />\n\
      <set x="9" value="25.2" />\n\
      <set x="10" value="25.31" />\n\
      <set x="11" value="25.28" /> \n\
      <set x="12" value="25.52" /> \n\
      <set x="13" value="25.7" /> \n\
      <set x="14" value="25.9" /> \n\
      <set x="15" value="26" />\n\
   </trendset>\n\
   <trendLines>\n\
      <line startValue="24.2" color="0372AB" displayvalue="S1" thickness="0.5" dashed="1" dashLen="2" dashGap="2" />\n\
      <line startValue="23.35" color="0372AB" displayvalue="S2" thickness="0.5" dashed="1" dashLen="2" dashGap="2" />\n\
      <line startValue="28.2" color="0372AB" displayvalue="R2" thickness="0.5" dashed="1" dashLen="2" dashGap="2" />\n\
      <line startValue="27.65" color="0372AB" displayvalue="R1" thickness="0.5" dashed="1" dashLen="2" dashGap="2" />\n\
   </trendLines>\n\
   <vtrendLines>\n\
      <line startValue="10" endValue="13" color="FF5904" displayvalue="Results Impact" isTrendZone="1" alpha="10" />\n\
   </vtrendLines>\n\
</chart>';
xml['SSGrid'] = "<chart caption='Monthly Unit Sales' xAxisName='Month' yAxisName='Units' showValues='0'>\n\
    <set label='Jan' value='462' />\n\
    <set label='Feb' value='857' />\n\
    <set label='Mar' value='671' />\n\
    <set label='Apr' value='494' />\n\
    <set label='May' value='761' />\n\
    <set label='Jun' value='960' />\n\
    <set label='Jul' value='629' />\n\
    <set label='Aug' value='622' />\n\
    <set label='Sep' value='376' />\n\
    <set label='Oct' value='494' />\n\
    <set label='Nov' value='761' />\n\
    <set label='Dec' value='960' />\n\
</chart>";

xml['Kagi'] = "<chart showValues='0'>\n\
   <set label='3/1' value='25.49' />\n\
   <set label='3/2' value='25.11' />\n\
   <set label='3/3' value='26.35' />\n\
   <set label='3/4' value='28.07' />\n\
   <set label='3/5' value='25.36' />\n\
   <set label='3/6' value='26.3' />\n\
   <set label='3/7' value='26.61' />\n\
   <set label='3/8' value='25.58' />\n\
   <set label='3/9' value='27.8' />\n\
   <set label='3/10' value='28.16' />\n\
   <set label='3/11' value='25.44' />\n\
   <set label='3/12' value='27.3' />\n\
   <set label='3/13' value='28.85' />\n\
   <set label='3/14' value='26.29' />\n\
   <set label='3/15' value='25.23' />\n\
   <set label='3/16' value='28.43' />\n\
   <set label='3/17' value='28.63' />\n\
   <set label='3/18' value='25.66' />\n\
   <set label='3/19' value='28.15' />\n\
   <set label='3/20' value='26.81' />\n\
   <set label='3/21' value='25.32' />\n\
   <set label='3/22' value='27.76' />\n\
   <set label='3/23' value='26.54' />\n\
   <set label='3/24' value='27.1' />\n\
   <set label='3/25' value='27.69' />\n\
   <set label='3/26' value='28.67' />\n\
   <set label='3/27' value='25.88' />\n\
   <set label='3/28' value='26.76' />\n\
   <set label='3/29' value='28.41' />\n\
   <set label='3/30' value='25.52' />\n\
</chart>";

xml['HeatMap'] = "<chart Caption='PR Visibility (articles)' xAxisName='Week Days' yAxisName='Companies'>\n\
    <dataset>\n\
     <set rowId='Google' columnId='Mon' value='68'/>\n\
     <set rowId='Google' columnId='Tue' value='35'/>\n\
     <set rowId='Google' columnId='Wed' value='95'/>\n\
     <set rowId='Google' columnId='Thu' value='17'/>\n\
     <set rowId='Google' columnId='Fri' value='55'/>\n\
     <set rowId='Yahoo' columnId='Mon' value='0'/>\n\
     <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
     <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
     <set rowId='Yahoo' columnId='Thu' value='63'/>\n\
     <set rowId='Yahoo' columnId='Fri' value='79'/>\n\
     <set rowId='Microsoft' columnId='Mon' value='98'/>\n\
     <set rowId='Microsoft' columnId='Tue' value='48'/>\n\
     <set rowId='Microsoft' columnId='Wed' value='31'/>\n\
     <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
     <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
   </dataset>\n\
  <colorRange gradient='1' minValue='0' code='B63300' startlabel='Very Bad' endLabel='Very Good'>\n\
        <color code ='FFCC00'  maxValue='30' label='BAD'/>\n\
        <color code ='87B213' maxValue='70' label='AVERAGE'/>\n\
        <color code ='006384' maxValue='100' />\n\
  </colorRange>\n\
</chart>";

xml['Funnel'] = "<chart caption='Conversion Ratio' subcaption='May 2007' >\n\
   <set label='Website Visits' value='385634' />\n\
   <set label='Downloads' value='175631' />\n\
   <set label='Interested to buy' value='84564' />\n\
   <set label='Contract finalized' value='35654' />\n\
   <set label='Purchased' value='12342' />\n\
   <styles>\n\
      <definition>n\n\
         <style type='font' name='captionFont' size='15' />\n\
      </definition>\n\
      <application>\n\
      <apply toObject='CAPTION' styles='captionFont' />\n\
      </application>\n\
   </styles>\n\
</chart>";

xml['Pyramid'] = "<chart caption='Sales distribution by Employee' subCaption='Jan 07 - Jul 07' numberPrefix='$'>\n\
   <set label='Buchanan' value='20000' />\n\
   <set label='Callahan' value='49000' />\n\
   <set label='Davolio' value='63000' />\n\
   <set label='Dodsworth' value='41000' />\n\
   <set label='Fuller' value='74000' />\n\
   <set label='King' value='49000' />\n\
   <set label='Leverling' value='77000' />\n\
   <set label='Peacock' value='54000' />\n\
   <set label='Suyama' value='14000' />\n\
</chart>";

xml['SparkLine'] = "<chart caption='Cisco'>\n\
   <dataset>\n\
      <set value='27.26' />\n\
      <set value='37.88' />\n\
      <set value='38.88' />\n\
      <set value='22.9' />\n\
      <set value='39.02' />\n\
      <set value='23.31' />\n\
      <set value='30.85' />\n\
      <set value='27.01' />\n\
      <set value='33.2' />\n\
      <set value='21.93' />\n\
      <set value='34.51' />\n\
      <set value='24.84' />\n\
      <set value='39.32' />\n\
      <set value='37.04' />\n\
      <set value='27.81' />\n\
      <set value='22.95' />\n\
      <set value='24.73' />\n\
      <set value='37.63' />\n\
      <set value='29.75' />\n\
      <set value='22.35' />\n\
      <set value='34.35' />\n\
      <set value='27.6' />\n\
      <set value='27.97' />\n\
      <set value='32.36' />\n\
      <set value='22.56' />\n\
      <set value='24.15' />\n\
      <set value='24.93' />\n\
      <set value='35.82' />\n\
      <set value='23.45' />\n\
      <set value='37.64' />\n\
      <set value='26.99' />\n\
      <set value='29.48' />\n\
      <set value='36.63' />\n\
      <set value='35.58' />\n\
      <set value='32.19' />\n\
      <set value='27.59' />\n\
      <set value='26.94' />\n\
      <set value='32.35' />\n\
      <set value='22.63' />\n\
      <set value='25.97' />\n\
      <set value='25.28' />\n\
      <set value='26.73' />\n\
      <set value='23.47' />\n\
      <set value='20.55' />\n\
      <set value='34.58' />\n\
      <set value='29.16' />\n\
      <set value='34.97' />\n\
      <set value='24.57' />\n\
      <set value='20.7' />\n\
      <set value='32.61' />\n\
   </dataset>\n\
</chart>";

xml['SparkColumn'] = "<chart caption='Revenue' subcaption='12 months' numberPrefix='$' highColor='00CC33' lowColor='CC0000'>\n\
   <dataset>\n\
      <set value='783000' />\n\
      <set value='201000' />\n\
      <set value='515000' />\n\
      <set value='115900' />\n\
      <set value='388000' />\n\
      <set value='433000' />\n\
      <set value='910000' />\n\
      <set value='198000' />\n\
      <set value='183300' />\n\
      <set value='162000' />\n\
      <set value='159400' />\n\
      <set value='185000' />\n\
   </dataset>\n\
</chart>";

xml['SparkWinLoss'] = "<chart caption='Yankees' subcaption='Current season' >\n\
   <dataset>\n\
      <set value='W' />\n\
      <set value='W' />\n\
      <set value='D' />\n\
      <set value='L' />\n\
      <set value='W' />\n\
      <set value='W' />\n\
      <set value='L' />\n\
      <set value='L' />\n\
      <set value='W' />\n\
      <set value='L' />\n\
      <set value='W' scoreless='1'/>\n\
      <set value='L' />\n\
      <set value='W' />\n\
      <set value='W' />\n\
   </dataset>\n\
</chart>";

xml['HBullet'] = "<chart lowerLimit='0' upperLimit='100' caption='Revenue' subcaption='US $ (1,000s)' numberPrefix='$' numberSuffix='K' showValue='1' >\n\
   <colorRange>\n\
      <color minValue='0' maxValue='50' color='A6A6A6'/>\n\
      <color minValue='50' maxValue='75' color='CCCCCC'/> \n\
      <color minValue='75' maxValue='100' color='E1E1E1'/> \n\
   </colorRange> \n\
   <value>78.9</value>\n\
   <target>80</target>\n\
</chart>";

xml['VBullet'] = "<chart lowerLimit='0' upperLimit='100' caption='Revenue' subcaption='US $ (1,000s)' numberPrefix='$' numberSuffix='K' showValue='1' >\n\
   <colorRange>\n\
      <color minValue='0' maxValue='50' color='A6A6A6'/>\n\
      <color minValue='50' maxValue='75' color='CCCCCC'/> \n\
      <color minValue='75' maxValue='100' color='E1E1E1'/> \n\
   </colorRange> \n\
   <value>78.9</value>\n\
   <target>80</target>\n\
</chart>";

xml['AngularGauge'] = "<Chart bgColor='FFFFFF' upperLimit='100' lowerLimit='0' showLimits='1' baseFontColor='666666'  majorTMNumber='11' majorTMColor='666666'  majorTMHeight='8' minorTMNumber='5' minorTMColor='666666' minorTMHeight='3' pivotRadius='20' showGaugeBorder='0' gaugeOuterRadius='100' gaugeInnerRadius='90' gaugeOriginX='170' gaugeOriginY='170' gaugeScaleAngle='320' displayValueDistance='10' placeValuesInside='1' gaugeFillMix='' pivotFillMix='{F0EFEA}, {BEBCB0}' pivotBorderColor='BEBCB0' pivotfillRatio='80,20'>\n\
	<colorRange>\n\
		<color minValue='0' maxValue='80' code='00FF00' alpha='0'/>\n\
		<color minValue='80' maxValue='100' name='Danger' code='FF0000' alpha='50'/>\n\
	</colorRange>\n\
	<dials>\n\
		<dial value='65' bordercolor='FFFFFF' bgColor='bebcb0, f0efea, bebcb0' borderAlpha='0' baseWidth='10' topWidth='3'/>\n\
	</dials>\n\
	<annotations>\n\
		<annotationGroup xPos='170' yPos='170' fillRatio='10,125,254' fillPattern='radial' >\n\
			<annotation type='circle' xPos='0' yPos='0' radius='150' borderColor= 'bebcb0' fillAsGradient='1' fillColor='f0efea, bebcb0'  fillRatio='85,15'/>\n\
			<annotation type='circle' xPos='0' yPos='0' radius='120' fillColor='bebcb0, f0efea' fillRatio='85,15' />\n\
			<annotation type='circle' xPos='0' color='FFFFFF' yPos='0' radius='100' borderColor= 'f0efea' />\n\
		</annotationGroup>\n\
	</annotations>\n\
</Chart>";

xml['HLinearGauge'] = "<chart lowerLimit='0' upperLimit='100' lowerLimitDisplay='Bad' upperLimitDisplay='Good' palette='1' numberSuffix='%' chartRightMargin='20'>\n\
   <colorRange>\n\
      <color minValue='0' maxValue='75' code='FF654F' label='Bad'/>\n\
      <color minValue='75' maxValue='90' code='F6BD0F' label='Moderate'/>\n\
      <color minValue='90' maxValue='100' code='8BBA00' label='Good'/>\n\
   </colorRange>\n\
   <pointers>\n\
      <pointer value='92' />\n\
   </pointers>\n\
</chart>";

xml['VLinearGauge'] = "<chart lowerLimit='0' upperLimit='100' lowerLimitDisplay='Bad' upperLimitDisplay='Good' palette='1' numberSuffix='%' chartRightMargin='20'>\n\
   <colorRange>\n\
      <color minValue='0' maxValue='75' code='FF654F' label='Bad'/>\n\
      <color minValue='75' maxValue='90' code='F6BD0F' label='Moderate'/>\n\
      <color minValue='90' maxValue='100' code='8BBA00' label='Good'/>\n\
   </colorRange>\n\
   <pointers>\n\
      <pointer value='92' />\n\
   </pointers>\n\
</chart>";

xml['SparkLine'] = "<chart caption='Cisco' canvasLeftMargin='110'>\n\
	<dataset>\n\
		<set value='38.42' />\n\
		<set value='41.43' />\n\
		<set value='34.78' />\n\
		<set value='40.67' />\n\
		<set value='44.12' />\n\
		<set value='38.45' />\n\
		<set value='40.71' />\n\
		<set value='49.90' />\n\
		<set value='40.12' />\n\
		<set value='34.91' />\n\
		<set value='42.02' />\n\
		<set value='35.21' />\n\
		<set value='43.31' />\n\
		<set value='40.21' />\n\
		<set value='40.54' />\n\
		<set value='40.90' />\n\
		<set value='54.21' />\n\
		<set value='41.90' />\n\
		<set value='33.43' />\n\
		<set value='46.73' />\n\
		<set value='50.42' />\n\
		<set value='40.74' />\n\
		<set value='42.31' />\n\
		<set value='50.39' />\n\
		<set value='51.10' />\n\
		<set value='44.84' />\n\
		<set value='51.64' />\n\
		<set value='47.62' />\n\
		<set value='39.61' />\n\
		<set value='35.13' />\n\
	</dataset>\n\
</chart>";

xml['HLED'] = "<chart lowerLimit='0' upperLimit='120' lowerLimitDisplay='Low' upperLimitDisplay='High' palette='1' numberSuffix='dB' chartRightMargin='20' >\n\
   <colorRange>\n\
      <color minValue='0' maxValue='60' code='FF0000' />\n\
      <color minValue='60' maxValue='90' code='FFFF00' />\n\
      <color minValue='90' maxValue='120' code='00FF00' />\n\
   </colorRange>\n\
   <value>102</value>\n\
</chart>";


xml['VLED'] = "<chart lowerLimit='0' upperLimit='120' lowerLimitDisplay='Low' upperLimitDisplay='High' palette='1' numberSuffix='dB' chartRightMargin='20' >\n\
   <colorRange>\n\
      <color minValue='0' maxValue='60' code='FF0000' />\n\
      <color minValue='60' maxValue='90' code='FFFF00' />\n\
      <color minValue='90' maxValue='120' code='00FF00' />\n\
   </colorRange>\n\
   <value>102</value>\n\
</chart>";

xml['Thermometer'] = "<chart palette='4' lowerLimit='-50' upperLimit='10' numberSuffix='° C'>\n\
   <value>-40</value>\n\
</chart>";

xml['Cylinder'] = "<chart palette='3' lowerLimit='0' upperLimit='4000' numberSuffix=' ltrs.' bgColor='FFFFFF'>\n\
   <value>2452</value>\n\
</chart>";

xml['Bulb'] = "<chart upperLimit='100' lowerLimit='0' numberSuffix='%'>\n\
   <colorRange>\n\
      <color minValue='0' maxValue='15' label='Low' code='00FF00' />\n\
      <color minValue='15' maxValue='35' label='Medium' code='FFFF00' />\n\
      <color minValue='35' maxValue='100' label='High' code='FF0000' />\n\
   </colorRange>\n\
   <value>12</value>\n\
</chart>";

xml['Gantt'] = "<chart dateFormat='mm/dd/yyyy' caption='Project Gantt' subCaption='From 1st Feb 2007 - 31st Aug 2007'>\n\
   <categories>\n\
      <category start='02/01/2007' end='03/01/2007' label='Feb' />\n\
      <category start='03/01/2007' end='04/01/2007' label='Mar' />\n\
      <category start='04/01/2007' end='05/01/2007' label='Apr' />\n\
      <category start='05/01/2007' end='06/01/2007' label='May' />\n\
      <category start='06/01/2007' end='07/01/2007' label='Jun' />\n\
      <category start='07/01/2007' end='08/01/2007' label='Jul' />\n\
      <category start='08/01/2007' end='09/01/2007' label='Aug' />\n\
   </categories>\n\
   <processes fontSize='12' isBold='1' align='right'>\n\
      <process label='Identify Customers' />\n\
      <process label='Survey 50 Customers' />\n\
      <process label='Interpret Requirements' />\n\
      <process label='Study Competition' />\n\
      <process label='Documentation of features' />\n\
      <process label='Brainstorm concepts' />\n\
      <process label='Design & Code' />\n\
      <process label='Testing / QA' />\n\
      <process label='Documentation of product' />\n\
      <process label='Global Release' />\n\
   </processes>\n\
   <tasks>\n\
      <task start='02/04/2007' end='02/10/2007' />\n\
      <task start='02/08/2007' end='02/19/2007' />\n\
      <task start='02/19/2007' end='03/02/2007' />\n\
      <task start='02/24/2007' end='03/02/2007' />\n\
      <task start='03/02/2007' end='03/21/2007' />\n\
      <task start='03/21/2007' end='04/06/2007' />\n\
      <task start='04/06/2007' end='07/21/2007' />\n\
      <task start='07/21/2007' end='08/19/2007' />\n\
      <task start='07/28/2007' end='08/24/2007' />\n\
      <task start='08/24/2007' end='08/27/2007' /> \n\
   </tasks>\n\
</chart>";


xml['DrawingPad'] = "<chart bgColor='E1F5FF'>\n\
   <annotations>\n\
      <annotationGroup id='Grp1' >\n\
         <annotation type='rectangle' x='30' y='100' toXPos='110' toYPos='220' radius='5' color='453269' />\n\
         <annotation type='rectangle' x='235' y='100' toXPos='315' toYPos='220' radius='5' color='453269' />\n\
         <annotation type='rectangle' x='440' y='100' toXPos='520' toYPos='220' radius='5' color='453269' />\n\
      </annotationGroup>\n\
      <annotationGroup id='Grp2'>\n\
         <annotation type='text' x='70' y='155' fontSize='12' isBold='1' label='Chart' color='FFFFFF'/>\n\
         <annotation type='text' x='275' y='140' fontSize='12' isBold='1' label='Scripts' color='FFFFFF'/>\n\
         <annotation type='text' x='275' y='170' label='ASP/PHP/.NET/.. pages' color='EFEBF5' wrap='1' wrapWidth='60'/>\n\
         <annotation type='text' x='480' y='155' fontSize='12' isBold='1' label='Database' color='FFFFFF'/>\n\
      </annotationGroup>\n\
      <annotationGroup id='Grp3' >\n\
         <annotation type='text' x='70' y='65' label='1. HTML provides the URL of XML data document to chart' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='170' y='130' label='2. Chart sends a request to the specified URL for XML data' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='380' y='130' label='3. These pages now interact with the database' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='380' y='200' label='4. Data returned back to the scipts in native objects' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='275' y='255' label='5. Scripts convert it into XML and finally output it' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='170' y='200' label='6. XML data is returned to the chart' wrap='1' wrapWidth='100' color='453269'/>\n\
         <annotation type='text' x='70' y='250' label='7. Chart is finally rendered' wrap='1' wrapWidth='100' color='453269'/>\n\
      </annotationGroup>\n\
      <annotationGroup id='Grp4'>\n\
         <annotation type='line' x='120' y='160' toX='220' color='453269' />\n\
         <annotation type='line' x='215' y='155' toX='220' toY='160' color='453269' />\n\
         <annotation type='line' x='215' y='165' toX='220' toY='160' color='453269' />\n\
         <annotation type='line' x='120' y='175' toX='220' color='453269' />\n\
         <annotation type='line' x='125' y='170' toX='120' toY='175' color='453269' />\n\
         <annotation type='line' x='125' y='180' toX='120' toY='175' color='453269' />\n\
         <annotation type='line' x='325' y='155' toX='435' color='453269' />\n\
         <annotation type='line' x='430' y='150' toX='435' toY='155' color='453269' />\n\
         <annotation type='line' x='430' y='160' toX='435' toY='155' color='453269' />\n\
         <annotation type='line' x='325' y='170' toX='435' color='453269' />\n\
         <annotation type='line' x='330' y='165' toX='325' toY='170' color='453269' />\n\
         <annotation type='line' x='330' y='175' toX='325' toY='170' color='453269' />\n\
      </annotationGroup>\n\
      <annotationGroup id='Grp5'>\n\
         <annotation type='text' label='FusionCharts dataURL method' fontSize='16' fontColor='666666' isBold='1' x='270' y='20'/>\n\
      </annotationGroup>\n\
   </annotations>\n\
</chart>";

xml['HeatMap'] = "<chart caption='Weekly Percentage' subCaption='In percentage'>\n\
    <rows>\n\
        <row id='Google' label='Google US'/>\n\
        <row id='Yahoo' label='Yahoo US'/>\n\
        <row id='Microsoft' label='Microsoft US'/>\n\
    </rows>\n\
    <columns>\n\
        <column id='MON' label='Monday'/>\n\
        <column id='TUE' label='Tuesday'/>\n\
        <column id='WED' label='Wednesday'/>\n\
        <column id='THU' label='Thursday'/>\n\
        <column id='FRI' label='Friday'/>\n\
    </columns>\n\
    <dataset>\n\
        <set rowId='Yahoo' columnId='Mon' value='0' />\n\
        <set rowId='Yahoo' columnId='Tue' value='71'/>\n\
        <set rowId='Yahoo' columnId='Wed' value='70'/>\n\
        <set rowId='Yahoo' columnId='Thu' value='3'/>\n\
        <set rowId='Yahoo' columnId='Fri' value='79' />\n\
        \n\
        <set rowId='Google' columnId='Mon' value='68'/>\n\
        <set rowId='Google' columnId='Tue' value='5'/>\n\
        <set rowId='Google' columnId='Wed' value='95'/>\n\
        <set rowId='Google' columnId='Thu' value='-10'/>\n\
        <set rowId='Google' columnId='Fri' value='55.98'/>\n\
        \n\
        <set rowId='Microsoft' columnId='Mon' value='98' />\n\
        <set rowId='Microsoft' columnId='Tue' value='8'/>\n\
        <set rowId='Microsoft' columnId='Wed' value='123'/>\n\
        <set rowId='Microsoft' columnId='Thu' value='79'/>\n\
        <set rowId='Microsoft' columnId='Fri' value='39'/>\n\
    </dataset>\n\
    <colorRange mapbypercent='1' gradient='1' minValue='0' code='00CCFF' startLabel='Poor' endLabel='Good'>\n\
        <color code ='66ADD9' maxValue='30' label='BAD'/>\n\
        <color code ='F2CF63' maxValue='70' label='AVERAGE'/>\n\
        <color code ='D99036' maxValue='100' />\n\
    </colorRange>\n\
</chart>";

xml['BoxAndWhisker2D'] = "<chart caption='Visits to a Website' subCaption='In three years' useRoundEdges='1'>\n\
\n\
<categories>\n\
    <category label='2008' />\n\
    <category label='2009' />\n\
    <category label='2010' />\n\
</categories>\n\
\n\
<dataset seriesName='January' lowerBoxColor='019FAA' upperboxColor='D4DFFF'>\n\
    <set value='60,133,35,67,89,137,259' />\n\
    <set value='139,197,175,114,39,67,191' />\n\
    <set value='200,107,197,239,53,26,97' />\n\
</dataset>\n\
\n\
<dataset seriesName='February' lowerBoxColor='2A5F55' upperboxColor='D47F00'>\n\
    <set value='160,303,125,137,69,107,39' />\n\
    <set value='139,167,255,74,59,187,151' />\n\
    <set value='160,137,57,209,153,126,277' />\n\
</dataset>\n\
   \n\
</chart>";

xml['RealTimeArea'] =
xml['RealTimeColumn'] =
xml['RealTimeColumn3D'] =
xml['RealTimeLine'] = '<chart caption="Realtime chart"\n\
    showRealTimeValue="0" dataStreamURL="testfiles/StockPrice.asp?l=10&u=20"\n\
    refreshInterval="2" yAxisMaxValue="100" numDisplaySets="10"\n\
    labeldisplay="rotate" \n\
    numberSuffix="%" showLabels="1"> \n\
  <categories>\n\
    <category label="Start"/>\n\
  </categories>\n\
  <dataset color="00dd00" seriesName="Processor A" alpha="100" anchorAlpha="0" lineThickness="2">\n\
    <set value="0"/>\n\
  </dataset>\n\
  <dataset color="ff5904" seriesName="Processor B" alpha="100" anchorAlpha="0" lineThickness="2">\n\
    <set value="0"/>\n\
  </dataset>\n\
</chart>';

xml['RealTimeLineDY'] = '<chart palette="3" caption="Stock Price Monitor" subCaption="(Updates every 3 seconds) - Random data" \n\
    dataStreamURL="testfiles/RealTime3.asp" refreshInterval="3" \n\
    numDisplaySets="40" labelStep="2" labeldisplay="rotate" \n\
    numberPrefix="$" SNumberPrefix="$" setAdaptiveYMin="1" setAdaptiveSYMin="1" \n\
    xAxisName="Time" showRealTimeValue="1" labelDisplay="Rotate" slantLabels="1" \n\
    PYAxisMinValue="29" PYAxisMaxValue="36" SYAxisMinValue="21" SYAXisMaxValue="26">\n\
  <categories/>\n\
  <dataset seriesName="Google" />\n\
  <dataset seriesName="Dell" parentYAxis="S"/>\n\
  <styles>\n\
    <definition>\n\
      <style type="font" name="captionFont" size="14"/>\n\
    </definition>\n\
    <application>\n\
       <apply toObject="Realtimevalue" styles="captionFont"/>\n\
    </application>\n\
  </styles>\n\
  <trendlines>\n\
    <line parentYAxis="P" startValue="32.7" displayValue="Open" thickness="1" color="0372AB" dashed="1"/>\n\
    <line parentYAxis="S" startValue="22.5" displayValue="Open" thickness="1" color="DF8600" dashed="1"/>\n\
  </trendlines>\n\
</chart>';

xml['RealTimeStackedArea'] =
xml['RealTimeStackedColumn'] =
xml['RealTimeStackedColumn3D']= '<chart caption="Realtime Server Load"\n\
    yAxisMaxValue="100" numDisplaySets="30" showRealTimeValue="0" \n\
    dataStreamURL="testfiles/StockPrice.asp" refreshInterval="2"\n\
    numberSuffix="%" labelDisplay="rotate" slantLabels="1" showLabels="1" labeldisplay="rotate">\n\
  <categories/>\n\
  <dataset color="009900" seriesName="Client A" />\n\
  <dataset color="ffff00" seriesName="Client B (with values)" />\n\
  <dataset color="0099cc" seriesName="Client C" />\n\
</chart>';



// Custom MSStackedCombi chart

xml['msstackedcombidy2d'] = "<chart caption='Annual Revenue' subcaption='In Million $' xaxisname='Year' PYaxisname='Sales in M$' SYAxisName='Cost as % of Revenue' decimals='0' numberPrefix='$' numberSuffix='M' sNumberSuffix='%' >\n\
   <categories>\n\
      <category label='2001'/>\n\
      <category label='2002'/>\n\
      <category label='2003'/>\n\
      <category label='2004'/>\n\
      <category label='2005'/>\n\
   </categories>\n\
   <dataset renderAs='area' parentYAxis='s'>\n\
      <dataset seriesName='Product A' color='AFD8F8' >\n\
         <set value='30' />\n\
         <set value='26' />\n\
         <set value='29' />\n\
         <set value='31' />\n\
         <set value='34' />\n\
      </dataset>\n\
      <dataset seriesName='Product B' color='F6BD0F' >\n\
         <set value='21' />\n\
         <set value='28' />\n\
         <set value='39' />\n\
         <set value='41' />\n\
         <set value='24' />\n\
      </dataset>\n\
   </dataset>\n\
   <dataset>\n\
      <dataset seriesname='Service A' color='8BBA00' >\n\
         <set value='27' />\n\
         <set value='25' />\n\
         <set value='28' />\n\
         <set value='26' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service B' color='A66EDD' >\n\
         <set value='17' />\n\
         <set value='15' />\n\
         <set value='18' />\n\
         <set value='16' />\n\
         <set value='10' />\n\
      </dataset>\n\
      <dataset seriesname='Service C' color='F984A1' >\n\
         <set value='12' />\n\
         <set value='17' />\n\
         <set value='16' />\n\
         <set value='15' />\n\
         <set value='12' />\n\
      </dataset>\n\
   </dataset>\n\
   <dataset seriesname='C1' renderAs='line' drawInStepMode='1' useForwardSteps='1'>\n\
      <set value='7' />\n\
      <set value='8' />\n\
      <set value='9' />\n\
      <set value='3' />\n\
      <set value='1' />\n\
   </dataset>\n\
   <dataset seriesname='A1' renderAs='area' drawInStepMode='1' useForwardSteps='1' >\n\
      <set value='7' />\n\
      <set value='8' />\n\
      <set value='9' />\n\
      <set value='3' />\n\
      <set value='0' />\n\
   </dataset>\n\
   <dataset seriesname='A2' renderAs='area' drawInStepMode='1' useForwardSteps='1' >\n\
      <set value='19' />\n\
      <set value='13' />\n\
      <set value='12' />\n\
      <set value='23' />\n\
      <set value='23' />\n\
   </dataset>\n\
      <dataset seriesname='L1' lineThickness='4' renderAs='line' drawInStepMode='1' useForwardSteps='1' >\n\
      <set value='17' />\n\
      <set value='28' />\n\
      <set value='39' />\n\
      <set value='13' />\n\
      <set value='20' />\n\
   </dataset>\n\
   <dataset seriesname='L2' renderAs='line' drawInStepMode='1' useForwardSteps='1' >\n\
      <set value='27' />\n\
      <set value='38' />\n\
      <set value='39' />\n\
      <set value='23' />\n\
      <set value='20' />\n\
   </dataset>\n\
</chart>";


xml['jdacustomcombichart'] = xml['msstackedcombidy2d'];