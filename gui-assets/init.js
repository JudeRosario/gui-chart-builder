var querystring = parseQS(window.location.search),
    NoConsoleNotified = true;
    FC_DEV_ENVIRONMENT = true;

function mylog() {
    console && console.log && typeof console.log == 'function' &&
            console.log(arguments);
}

function eventRegsiterREDRendered() {
    RChart.addEventListener('rendered', function (e, a) {
        window.redchart = e.sender.ref.jsVars.hcObj;
        if (jQuery.cookie('exportenabled') === 'true') {
            e.sender.exportChart({
                exportAction: 'save'
            });
        }
    });

}


function parseQS(str, retainPropertyNameCase) {
    var qsArr = {};
    str.replace(
            new RegExp("([^?=&]+)(=([^&]*))?", "g"),
            function($0, $1, $2, $3) {
                qsArr[retainPropertyNameCase ? $1 : $1.toLowerCase()] = $3;
            });
    return qsArr;
}

// script for loading firebug lite when console.log is encountered.
if (!window.console) {
    window.console = {
        log: function(args) {
            if (!NoConsoleNotified) {
                alert("The script has developer outputs sent to JavaScript console. \n\
                        \nYour browser does not seem to either have console support or console is not enabled.");
                NoConsoleNotified = true;
            }
        }
    };
}

function loadLazyLib() {
    var libs = Array.prototype.slice.call(arguments),
            mergedPathList = [],
            cacheKiller = new Date().valueOf(),
            pathIndex, path, list,
            loadLib, libIndex, lib,
            fullPath, endcallback = function() {};

    if (typeof libs[libs.length-1] == "function") {
        endcallback = libs.pop();
    }

    for (pathIndex = 0; pathIndex < libs.length; pathIndex++) {
        path = libs[pathIndex]['path'];
        list = libs[pathIndex]['list'];
        loadLib = list.length > 0;

        //pre-processing
        if (loadLib) {
            for (libIndex = 0; libIndex < list.length; libIndex++) {
                lib = list[libIndex];
                fullPath = (/^\:/.test(lib)) && lib.replace(/^\:/, '') || path+lib;
                mergedPathList.push(fullPath+"?nocache="+ cacheKiller);
            }
        }

        //load js in sequence
        LazyLoad.js(mergedPathList, endcallback);
    }
}


function loadLib() {
    var libs = Array.prototype.slice.call(arguments),
            pathIndex, path, list, loadLib, libIndex, lib, soloLib, endcallback;
    if (typeof libs[libs.length-1] == "function") {
        endcallback = libs.pop();
    }

    for (pathIndex = 0; pathIndex < libs.length; pathIndex++) {
        path = libs[pathIndex]['path'];
        list = libs[pathIndex]['list'];
        loadLib = list.length > 0;
        $LAB.setOptions({ AlwaysPreserveOrder:true, AllowDuplicates: true, CacheBust:true });
        loadLib && (function() {
            for (libIndex = 0; libIndex < list.length; libIndex++) {
                lib = list[libIndex];
                soloLib = (lib.search(/^\:/) === 0);
                if (libIndex+1 === list.length && endcallback) {
                    $LAB.script(((soloLib && lib.replace(/^\:/, '')) || path + lib)).wait(
                        function () {
                            window.setTimeout( function () { endcallback(); }, 0);
                        }
                );
                } else {
                    $LAB.script(((soloLib && lib.replace(/^\:/, '')) || path + lib));
                }

            }
        }());
    }
}

function insertScript(doc, target, src, callback) {
    var s = doc.createElement("script");
    s.type = "text/javascript";
    if (callback) {
        if (s.readyState) {  //IE
            s.onreadystatechange = function() {
                if (s.readyState == "loaded" ||
                        s.readyState == "complete") {
                    s.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            s.onload = function() {
                callback();
            };
        }
    }
    s.src = src;
    target.appendChild(s);
}


var loadScriptFile = function(file, callback, scriptText, frame) {
      var ieVeresion = getIEVersion();

    // If file is not specified, we exit
    if (!file) {
        return false;
    }

    var context = frame && window.frames[frame] || window,
         contextDoc =    context.document || document,
         frameHead = contextDoc.getElementsByTagName('head')[0],
         script, src = file, scriptHTML;

    scriptHTML = "<script type='text/javascript' src='" + file + "'></script>";
    // Create the script element with its attributes.
    script = document.createElement('script')
    // Set the script type to javaScript
    script.type = 'text/javascript';
    // Set the prepared src as the script's src.
    script.src = src;
    // Set script inner text to what user passed as parameter.
    if (scriptText) {
        script[('\v' === 'v' ? 'text' : 'innerHTML')] = scriptText;
    }

    // Execute callback function when the script was loaded.
    if (typeof callback === 'function') {
        // Fix the infinite loop issue in IE9
        if ((jQuery.browser.msie && jQuery.browser.version < 8) || !jQuery.browser.msie) {
            script.onload = function() {
                window.setTimeout( function () { callback(); }, 0);
            };
        }
        script.onreadystatechange = function() {
            if (this.readyState === 'complete' || this.readyState === 'loaded') {
                window.setTimeout( function () { callback(); }, 0);
            }
        };
    }

    // Append the script to the head of this page.
    if (ieVeresion < 8) {

        frameHead.appendChild(script);

    }
    else {
        frameHead.appendChild(script);
    }

    return true;
};

function getIEVersion(){
    var isIE = /MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(navigator.userAgent),
        rv;

        if (isIE){
            rv = parseFloat(RegExp.$1);
        }
        return rv;
}
function loadNativeLib() {

    var libs = Array.prototype.slice.call(arguments),
            mergedPathList = [],
            cacheKiller = new Date().valueOf(),
            pathIndex, path, list,
            loadLib, libIndex, lib,
            fullPath, endcallback = function() {};

    if (typeof libs[libs.length-1] === "function") {
        endcallback = libs.pop();
    }

    for (pathIndex = 0; pathIndex < libs.length; pathIndex++) {
        path = libs[pathIndex]['path'];
        list = libs[pathIndex]['list'];
        loadLib = list.length > 0;

        //pre-processing
        if (loadLib) {
            for (libIndex = 0; libIndex < list.length; libIndex++) {
                lib = list[libIndex];
                fullPath = (/^\:/.test(lib)) && lib.replace(/^\:/, '') || path+lib;
                mergedPathList.push(fullPath+"?nocache="+ cacheKiller);
            }
        }

    }
    var loadScript = function (index) {
        if (index === undefined) {
            index = 0;
        }
        if (index >= mergedPathList.length) {
            window.setTimeout( function () { endcallback(); }, 0);
            return;
        }
        var scriptName = mergedPathList[index];

        loadScriptFile(scriptName, function () {
            loadScript(index + 1);
        });

    };

    loadScript();



}