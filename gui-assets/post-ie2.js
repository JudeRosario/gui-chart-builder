Object.keys = Object.keys || (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
        DontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        DontEnumsLength = DontEnums.length;

    return function (o) {
        if (typeof o != "object" && typeof o != "function" || o === null)
            throw new TypeError("Object.keys called on a non-object");

        var result = [];
        for (var name in o) {
            if (hasOwnProperty.call(o, name))
                result.push(name);
        }

        if (hasDontEnumBug) {
            for (var i = 0; i < DontEnumsLength; i++) {
                if (hasOwnProperty.call(o, DontEnums[i]))
                    result.push(DontEnums[i]);
            }
        }

        return result;
    };
})();

var getPermalink,
    firstLoad,
    cjar = {},
    flashActive = true,
    redActive = true,
    updateRedChart,
    updateFlashChart,

    RChart = {},
    HChart = {},
    chartDataJSON,
    chartDataXML,
    forcerenderCharts = false,
    cachedChartTypes = {},
    useRemoteExport,
    autosavepng,

    guidarkbg = function(state) {
        $(document.body).css({
            color: !state ? '#000000' : '#ffffff',
            backgroundColor: !state ? '#ffffff' : '#333344'
        });

    },

    loadIframeInitScripts = function (frameName, initLib, callback) {
        var context = window.frames[frameName].document,
                //scriptLoaderURL = "js/LABjs/LAB.src.js",
                scriptLoaderURL = "js/lazyload/lazyload-min.js",
                frameHead = context.getElementsByTagName('head').item(0);

        loadScriptFile('js/init.js' + "?nocache=" + new Date().valueOf(), function() {
            loadScriptFile(initLib + "?nocache=" + new Date().valueOf(), function() {
                callback && callback();
            }, null, frameName);
        }, null, frameName);

    },

    setcharttransp = function(state) {
        if (window.RChart && window.RChart.setTransparent) {

            RChart.setTransparent(state);
            RChart.isActive() && RChart.render();
        }
        if (window.HChart && HChart.setTransparent) {

            HChart.setTransparent(state);
            HChart.isActive() && HChart.render();
        }

    },

    isJSON = function(data) {
        return /^\s*?\{[\s\S]*\}\s*?$/g.test(data);
    },

    getPermalink = function() {
        location.href = "http://dev.intranet.fusioncharts.com/trunk/source/gui/renderer-comparison/?chart=" +
                $typeSelector.val() + '&chartAttributes=' + escape($attrTextArea.val());
        return false;
    },

    setJSONOrXML = function(chart, data) {
        var type = typeof data === 'object' ? 'JSON' : 'XML';
        var isJSON, isXML;
        isJSON = /^[\s\r\n\t]{0,}\{/.test(data);
        isXML = /^[\s\r\n\t]{0,}\</.test(data);

        window.chartDataJSON = type === 'JSON' ? data : FusionCharts.transcodeData(data, 'XML', 'JSON');
        window.chartDataXML = type === 'XML' ? data : FusionCharts.transcodeData(data, 'JSON', 'XML');


        $('#data-detected').parent().show();
        $('#data-detected').text(type + ' detected, ').stop(true, true).show();
        $('#data-forconvert').text('convert to ' + (isXML ? 'JSON' : 'XML'));

        chart && chart.setChartData && data && type && chart.setChartData(data, type);
    },

    loadCachedTypes = function() {
        var i,
            ctype,
            ctypeArr,
            ctypes = jQuery.cookie('cachedChartType');
       if (!ctypes) {
           return;
       }
       ctypes = ctypes.replace(/\s/g, '');

       ctypeArr = ctypes.split(',');
       for (var i in ctypeArr) {
           cachedChartTypes[ctypeArr[i]]= true;
       }
       showChartTypesFromCache();

    },

    addCachedChartType = function(ctype) {
        ctype = ctype.replace(/\s/g, '');
        if (cachedChartTypes[ctype]) {
            return;
        }

        cachedChartTypes[ctype] = true;
        jQuery.cookie('cachedChartType', Object.keys(cachedChartTypes).join(','));
        jQuery.cookie("selectedcharttype", ctype);

        showChartTypesFromCache();
    },

    showChartTypesFromCache = function() {

        var i,
            ctype,
            ctypeArr,
            ctypes = jQuery.cookie('cachedChartType');

        $("#cachedChartTypesDIV").html('');

       if (!ctypes) {
           return;
       }

       ctypeArr = ctypes.split(',');
       for (var i in ctypeArr) {
           cachedChartTypes[ctypeArr[i]]= true;
       }

        for (var ctype in cachedChartTypes) {
            if (cachedChartTypes[ctype]) {

                $("#cachedChartTypesDIV").append("<span class='ctype'><img src='gui-assets/images/close.gif' align='top' vspace='3'/> " + ctype + "&nbsp;</span>");
            }
        }

        $("#cachedChartTypesDIV .ctype").unbind('click').click(function() {

            jQuery('#chart-type').val($(this).text().replace(/\s/g, ''));
            jQuery('#chart-type').chosen().change();
            jQuery('#chart-type').trigger("liszt:updated");

        });
        $("#cachedChartTypesDIV .ctype img").unbind('click').click(function() {
            removeCachedChartType(this);
        });

    },

    removeCachedChartType = function(elm) {
        var elm = jQuery(elm).parent(),
            ctype = elm.text().replace(/\s/g, '');

        elm.remove();
        if (cachedChartTypes[ctype]) {
            delete cachedChartTypes[ctype];
        }

        jQuery.cookie('cachedChartType', Object.keys(cachedChartTypes).join(','));
    },

    FChartExport = function (ctype) {
        ctype = ctype || 'PNG';
        var ret, exportParams = {
            exportformat: ctype
        };

//
//        if (useRemoteExport == true) {
//            exportParams.exporthandler = "http://export.api3.fusioncharts.com/";
//            exportParams.exportaction = "download";
//        } else {
//            exportParams.exportaction = "save";
//        }

        RChart.exportChart(exportParams);

        if (ret === false) {
            alert("Export is not enabled.");
        }

    },

    exportCallback = function(a) {

        var holder = $('#exportHolder'),
            button = $('#collapseExportHandler'),
            status = a.statusCode,
            statusMsg = a.statusMessage || "",
            exportedfile = a.fileName || "",
            exportedformat = (/\..+?$/.exec(exportedfile)+"").toLowerCase(),
            notice = a.notice || "",
            html = "";

        if (!holder.is(':visible')) {
            holder.css({ display: 'block' });
        }
        if (!button.is(':visible')) {
            button.show();
        }


        html = "<div class='thumbparent'><div class='thumbholder'";
        html += " title='"+ exportedfile +
                (notice ? "\n(NOTE:"+ notice + ")" : "")+
                "'>";

        if (a.statusCode == "1") {

            if (exportedformat==='.pdf') {
                html += "<a href='" + exportedfile + "' target='_blank' >PDF: Open in new window</a>";
            }
            else if (exportedformat==='.svg') {
                html += "<a href='" + exportedfile + "' target='_blank' ><object type='image/svg+xml' data='" + exportedfile + "'></object></a>";
            }
            else {
                html += "<a href='" + exportedfile + "' target='_blank' >";
                html += "<img width='50' height='40' src='" + exportedfile +
                        "' onclick='openImageInWindow(\"" + exportedfile + "\")' style='width:50px;' />";
                html += "</a>";
            }
        }
        else {
            html += "<span style='color:red'>Error:<br>" + statusMsg +
                   (notice ? "<br>(NOTE:"+ notice + ")": "") +
                   "</span>";
        }

        html += "</div><div class='close' onclick='closeThumb(this);' title='Remove'></div></div>";

        holder.append(html);

        window.setTimeout( function ()  {
            holder.scrollTop(holder[0].scrollHeight-60);
        }, 100);

    },

    showHideExportHolder = function() {

        var isVisible = $('#exportHolder').is(':visible');
        if (isVisible) {
            $('#collapseExportHandler a span').html('V');
        }
        else {
             $('#collapseExportHandler a span').html('^');
        }
        $('#exportHolder').css({ display: isVisible ? 'none' : 'block' });

    },

    openImageInWindow = function(src) {

    },

    closeThumb = function(elm) {
        $(elm).parent().fadeOut( 'fast', function () {
            $(elm).parent().remove();
            if ($('#exportHolder').html() =="" && $('#exportHolder').is(':visible')) {
                showHideExportHolder();
                if ($('#collapseExportHandler').is(':visible')) {
                    $('#collapseExportHandler').hide();
                }
            }
        });

    },

    FC_Exported = function(a) {
        exportCallback(a);
    },

    extractAttrs = function () {
        var chart = window.RChart,
            data;

        if (!(chart && chart.dataReady())) {
            return;
        }
        data = chart.getJSONData();
        data = data && (data.chart || data.graph || data.map);

        if (!data) { return; }

        var ret = [];
        for (var key in data) {
            ret.push(key + "=\"" + data[key] + "\"");
        }

        if (!ret.length) {
            return;
        }
        ret = ret.join("\r\n");

        $('#chartAttributes').val(ret);
    },

    updateHCFrameJSModules = function() {
        window.HChart && window.HChart.dispose && window.HChart.dispose();
        HChart = null;
        loadIframeInitScripts("hc-chart-container", "js/gogo-highcharts.js");
    },

    updateUIFromCookie = function () {
        if (jQuery.cookie('jsdebugmode') === 'true') {
            cjar['jsdebugmode'] = 'true';
            FusionCharts.debugMode.enable(mylog, 'verbose');
            $('#jsdebugstate').attr('checked', 'checked');
        }
        if (jQuery.cookie('guidarkbg') === 'true') {
            cjar['guidarkbg'] = 'true';
            guidarkbg(true);
            $('#guidarkbgcb').attr('checked', 'checked');
        }
        if (jQuery.cookie('setcharttransp') === 'true') {
            cjar['setcharttransp'] = 'true';
            $('#setcharttranspcb').attr('checked', 'checked');
        }

        if (jQuery.cookie('activateflash') !== 'false') {
            cjar['activateflash'] = 'true';
            flashActive = true;
            $('.action-activate-flash').attr('checked', 'checked');
        }
        else {
            jQuery.cookie('activateflash', 'false');
            cjar['activateflash'] = 'false';
            flashActive = false;
            $('.action-activate-flash').attr('checked', undefined);
        }
        if (jQuery.cookie('activateRED') !== 'false') {
            redActive = true;
            cjar['activateRED'] = 'true';
            $('.action-activate-RED').attr('checked', 'checked');
        }
        else {
            jQuery.cookie('activateRED', 'false');
            cjar['activateRED'] = 'false';
            redActive = false;
            $('.action-activate-RED').attr('checked', undefined);
        }

        if (jQuery.cookie('enablefirebug') == 'true') {
            $('#enablefirebug').attr('checked', 'checked');
            cjar['enablefirebug'] = 'true';

        }

        if ($.cookie('autoformatdata') === 'true') {
            jQuery('#formatdata').attr('checked', 'checked');
            autoIndentData = true;
        }
        if ($.cookie('remoteexport') === 'true') {
           jQuery('#remoteexport').attr('checked', 'checked');
           useRemoteExport =  true;
        }

        if ($.cookie('autosavepng') === 'true') {
           jQuery('#autosavepng').attr('checked', 'checked');
           autosavepng =  true;
        }


         cjar['showflashchart'] = (jQuery.cookie('showflashchart') !== 'false');

            var isFCVisible = $('#flash-chart-container').is(":visible"),
                isHCVisible = $('#hc-chart-container').is(":visible");

            if( isFCVisible && isHCVisible ){
                $(".swapRendererBtn").show();
                if (cjar['showflashchart']) {
                    $(".showFlashBtn").click();
                }
                else {
                    $(".showJSBtn").click();
                }

            }
            else {

            }





    },

    initUIInteractivity = function() {

            jQuery('#formatdata').change(function(e) {
                autoIndentData = $(this).attr('checked') === 'checked';
                if (autoIndentData) {
                    $('.format-data-action').trigger('click', e);
                }
            });

        $('.action-activate-flash').click(function() {
            flashActive = $('.action-activate-flash').attr('checked') === 'checked';
            cjar['activateflash'] = flashActive+'';
            jQuery.cookie('activateflash', flashActive);

            if (!flashActive) {
                HChart && HChart.dispose && HChart.dispose();
                HChart = null;
            }
            else {
                createCharts(true, false);
                updateRedChart = false;
                $('#submit').click();
            }
        });

        $('.action-activate-RED').click(function() {
            redActive = ($('.action-activate-RED').attr('checked') === 'checked');
            jQuery.cookie('activateRED', redActive);
            cjar['activateRED'] = redActive+'';

            if (!redActive) {
                RChart && RChart.dispose && RChart.dispose();
                RChart = null;
            }
            else {
                updateFlashChart = false;
                createCharts(false, true);
                $('#submit').click();
            }
        });

        $('#enablefirebug').click(function() {
            if (confirm("GUI will reload for the change to take effect. Continue reload?")) {
                location.href = location.href;
            }
            else {
                return false;
            }
        });

        // Update chart data Realtime
        jQuery('#update-value').keyup(function(e) {
            if (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10) {
                var value = jQuery(this).val();

                if (HChart && HChart.feedData) {
                    HChart.feedData('value=' + value);
                }
                else if (HChart && HChart.ref.jsVars.hcObj.series[0].chart.realTimeUpdate) {
                    HChart.ref.jsVars.hcObj.series[0].chart.realTimeUpdate(value);
                }

                if (RChart && RChart.feedData) {
                    RChart.feedData('value=' + value);
                }
                else if (RChart && RChart.ref.jsVars.hcObj.series[0].chart.realTimeUpdate) {
                    RChart.ref.jsVars.hcObj.series[0].chart.realTimeUpdate(value);
                }
            }
        });

        jQuery('#rerender').click(function() {
            window.RChart && RChart.render && window.RChart.render();
            window.HChart && HChart.render && window.HChart.render();
            $('#event-target').text('');
        });

        $(".showJSBtn").click(function() {
            jQuery.cookie('showflashchart', 'false');

            $(".showJSBtn").hide();
            $(".showFlashBtn").show();

            $("#flash-chart-container").hide();
            $("#hc-chart-container").css({display: 'block'});


            updateRedChart = firstLoad;
            createCharts(true, false);
            $('#submit').click();



        });
        $(".showFlashBtn").click(function() {

            jQuery.cookie('showflashchart', 'true');

            $(".showFlashBtn").hide();
            $(".showJSBtn").show();

            $("#hc-chart-container").hide();
            $("#flash-chart-container").css({display: 'block'});

            updateRedChart = firstLoad;

            createCharts(true, false);
            $('#submit').click();


        });
    },


    createCharts = function(flash, js) {
        var hContainer = jQuery('#hc-chart-container'),
            rContainer = jQuery('#red-chart-container-div'),
            fContainer = jQuery('#flash-chart-container');


        if (js && rContainer.is(":visible") && FusionCharts) {

            RChart && RChart.dispose && RChart.dispose();
            RChart=null;

            RChart = new FusionCharts({
                'type': jQuery('#chart-type').val(),
                'id': 'newjschart',
                'width': '100%', 'height': '100%',
                'renderer': 'javascript',
                'renderAt': 'red-chart-container-div',
                'datasource': jQuery('#chartxml').val(),
                'dataformat': 'xml'
            });

            eventRegsiterREDRendered();

        }
    },

    initUI = function(scriptBaseURI) {

        // window.FusionCharts.options.scriptBaseUri = './fusioncharts/';
        // window.FusionCharts.options.html5ScriptNamePrefix = 'FusionCharts.HC.';
        window.FCLoaded = true;

        FusionCharts.addEventListener('exported', function (e,a) {
            //exportCallback(a);
        });

        FusionCharts.addEventListener('drawcomplete', function (e,a) {
            a.drawingLatency &&
               $('#some-status').text(a.drawingLatency + " ms");
        });

        // Chart default Height and Width
        var ChartHEIGHT = '350',
            ChartWIDTH = '600',
            debugMode = 0;


            autoIndentData = false,
            flashNoFallback = {
                'HorizontalErrorBar2D': true
            },
            cookey = {
                selectedType: 'selectedcharttype',
                staticData: 'staticxmlattrs',
                cacheXML: 'cachethexml',
                dataXML: 'dataxml',
                animation: 'animation',
                selectedData: 'selecteddataxml',
                fHeight: 'f-height',
                fWidth: 'f-width',
                jHeight: 'j-height',
                jWidth: 'j-width',
                firebug: 'enablefirebug'
            };

            firstLoad = true;



        FusionCharts._doNotLoadExternalScript({
            jquery: false
        });

    //    FusionCharts.debugMode.outputTo(function() {
    //            //console.log.apply(console, arguments);
    //    });
    //    FusionCharts.debugMode.outputFormat('verbose');
    //

        initUIInteractivity();
        updateUIFromCookie();



    //    FusionCharts.printManager.enabled(true)
    //    FusionCharts.debugMode.enabled(true, console.log, 'verbose');
        //FusionCharts.debugMode._enableFirebugLite();


        jQuery(document).ready(function() {
            var $dataTextArea = jQuery('#chartxml'),
                $attrTextArea = jQuery('#chartAttributes'),
                $typeSelector = jQuery('#chart-type'),
                $submit = jQuery('#submit');


            loadCachedTypes();

            $dataTextArea.bind('blur', function(e) {
                if (autoIndentData) {
                    $('.format-data-action').trigger('click', e);
                }
            });

            $('.reset-data-action').click(function(event) {
                var isMap = /maps\//.test($typeSelector.find('option:selected').parent().attr('rel'));
                $dataTextArea.val(xml[$typeSelector.val()] || isMap && '<map showBevel="0"></map>' || '')
                $dataTextArea.focus();
                if (event.shiftKey) {
                    $submit.click();
                }
            });

            $('.format-data-action').click(function(event) {
                var data = $dataTextArea.val(),
                        pfx = isJSON(data) ? 'json' : 'xml';
                $dataTextArea.val(vkbeautify[pfx](data));
            });

            $typeSelector.change(function() {
                var chartType = jQuery(this).val(),
                        isMap = /maps\//.test(jQuery(this).find('option:selected').parent().attr('rel'));

                jQuery.cookie(cookey.selectedType, chartType);
                if (jQuery.cookie(cookey.cacheXML)) {
                    $dataTextArea.val(jQuery.cookie(cookey.dataXML));
                } else {
                    $dataTextArea.val(xml[chartType] || isMap && '<map showBevel="0"></map>' || '');
                }
                //$dataTextArea.val(xml[chartType]);
                $submit.click();
                addCachedChartType(chartType);


            });

            $dataTextArea.keyup(function(e) {
                if (e.ctrlKey && (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10)) {
                    $submit.click();
                    e.preventDefault();
                    return false;
                }
            });

            $attrTextArea.keyup(function(e) {
                if (e.ctrlKey && (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10)) {
                    $submit.click();
                    return false;
                }
            });

            if (jQuery.cookie(cookey.selectedType)) {
                var ctype = jQuery.cookie(cookey.selectedType);
                $typeSelector.val(ctype);

            }

            $dataTextArea.val(xml[$typeSelector.val()]);
            $attrTextArea.val(jQuery.cookie(cookey.staticData) || '');

            if (jQuery.cookie('debugmode') == 'true') {
                debugMode = 1;
                $('#debugmode').attr('checked', 'checked');
            }

            createCharts(flashActive, redActive);


            jQuery('#submit').click(function() {

                var chartDATA = $dataTextArea.val(),
                    dataType = isJSON(chartDATA) ? 'JSON' : 'XML',
                    i,
                    jsonData = FusionCharts.transcodeData(chartDATA, dataType, 'JSON'),
                    chartAttr = FusionCharts.transcodeData('<chart ' + $attrTextArea.val() + '/>', 'xml', 'json').chart || {},
                    chartTAG = jsonData.chart || jsonData.map || jsonData.graph,
                    html5ExportHandler,
                    exportHandler,
                    exportAction;

                $('#data-detected').text('(' + dataType + ' detected)').stop(true, true).show().fadeOut(3000);

                // Error in parsing XML
                if (!chartTAG) {
                    jsonData.chart = chartTAG = {};
                }
                if (jQuery.cookie(cookey.animation) != 0) {
                    chartAttr.animation = 0;
                }

                if (autosavepng) {
                    chartAttr['exportenabled'] = 1;
                }
//                html5ExportHandler = chartAttr['html5exporthandler'];
//                exportHandler = chartAttr['exporthandler'];
//                exportAction = chartAttr['exportatclient'];
//
//                // Added export related attributes
//                html5ExportHandler !== undefined || (chartAttr['html5exporthandler'] = 'ExportHandler/index.php');
//                exportHandler !== undefined || (exportHandler = chartAttr['exporthandler'] = 'ExportHandler/index.php');
//                chartAttr['exportenabled'] !== undefined || (chartAttr['exportenabled'] = '1');
//                chartAttr['exportparameters'] !== undefined || (chartAttr['exportparameters'] = '');
//                exportAction !== undefined || (chartAttr['exportatclient'] = '0');

//                if (exportAction !== undefined) {
//                    if (/^https?\:\/\/(?!localhost|127\.)/.test(exportHandler)) {
//                        chartAttr['exportaction'] = 'download';
//                    }
//                    else {
//                        chartAttr['exportaction'] = 'save';
//                    }
//                }


                for (i in chartAttr) {
                    chartTAG[i] = chartAttr[i];
                }
                chartDATA = jsonData;



                if (dataType == 'XML') {
                    chartDATA = FusionCharts.transcodeData(jsonData, 'json', 'xml');
                }


                var chartType = $typeSelector.val();

                jQuery.cookie('selecteddataxml', jQuery('#chartxml').val());
                jQuery.cookie(cookey.staticData, jQuery('#chartAttributes').val());

                if (redActive && updateRedChart !== false) {

                    RChart && RChart.chartType && RChart.chartType($typeSelector.val(), true);
                    if (RChart!=undefined && RChart.isActive && !RChart.isActive()) {
                        setJSONOrXML(RChart, chartDATA);
                        resizeChart();
                        RChart.render();

                    }
                    else {
                        if (forcerenderCharts) {
                            if (RChart && RChart.clone) {
                                var temphc = RChart.clone({
                                    id: RChart.id
                                }, true);
                                RChart.dispose();
                                RChart = null;
                                RChart = new FusionCharts(temphc);
                                eventRegsiterREDRendered();
                                setJSONOrXML(RChart, chartDATA);
                                RChart.render();
                            }


                        }
                        else {
                            setJSONOrXML(RChart, chartDATA);

                        }
                    }

                }
                else {
                    updateRedChart = null;
                }


                if (!firstLoad) {
                    cacheXMLData();
                }
                firstLoad = false;

//                if (FusionCharts('oldflashchart')) {
//                    if (window.FCChartTypeSelected === chartType) {
//                        setJSONOrXML(FusionCharts('oldflashchart'), chartDATA);
//                    } else {
//                        $(".showFlashBtn").click();
//                        window.FCChartTypeSelected = chartType;
//                    }
//
//                }


            });



            // pre-select chart
            $typeSelector.find('option').each(function(index, item) {
                if ($(item).attr('value').toLowerCase() ==
                        (querystring['chart'] &&
                                querystring['chart'].toLowerCase())) {
                    $typeSelector.find('option:selected').removeAttr('selected');
                    $(item).attr('selected', 'selected');
                }
            });

            // pre add data from qs
            if (querystring['chartattributes']) {
                $('#chartAttributes').val(unescape(querystring['chartattributes']));
            }

            $typeSelector.change();

            if (jQuery.cookie('setcharttransp') === 'true') {
                setcharttransp(true);
                $('#setcharttranspcb').attr('checked', 'checked');
            }
            if (jQuery.cookie('forcerender') === 'true') {
                forcerenderCharts = true;
                cjar['forcerender'] = true;
                $('#forcerendercb').attr('checked', 'checked');
            }

            jQuery('#cachedata').change(cacheXMLData);

            function cacheXMLData() {
                if (jQuery('#cachedata').is(':checked')) {
                    jQuery.cookie(cookey.cacheXML, 1);
                    jQuery.cookie(cookey.dataXML, jQuery('#chartxml').val());
                } else {
                    jQuery.cookie(cookey.cacheXML, "");
                    jQuery.cookie(cookey.dataXML, "");
                }
            }

            if (jQuery.cookie(cookey.cacheXML)) {
                jQuery("#cachedata").attr("checked", "checked");
                jQuery("#chartxml").val(jQuery.cookie(cookey.dataXML));
                //jQuery('#submit').click();
            }

            jQuery('#animation').change(function() {
                if (jQuery('#animation').is(':checked')) {
                    jQuery.cookie(cookey.animation, 1);
                } else {
                    jQuery.cookie(cookey.animation, 0);
                }
            });

            if (jQuery.cookie(cookey.animation) != 0) {
                jQuery("#animation").attr("checked", "checked");
            }

            jQuery('#debugmode').click(function() {
                jQuery.cookie('debugmode', jQuery(this).attr('checked') == 'checked');
                // Store the xml in temp cookie
                // will use the same xml after the page gets reloaed.
                //jQuery.cookie('tempXMLCache', jQuery('#chartxml').val());
                //document.location.reload();
            });

            $angle = jQuery("span#angle");



            // Adjust the chart height and widht
            var timeOut, DELAY = 1000;
            // Cache the chart new widht and height
            jQuery('#j-width').val(jQuery.cookie(cookey.jWidth) || ChartWIDTH);
            jQuery('#j-height').val(jQuery.cookie(cookey.jHeight) || ChartHEIGHT);
            jQuery('#f-width').val(jQuery.cookie(cookey.fWidth) || ChartWIDTH);
            jQuery('#f-height').val(jQuery.cookie(cookey.fHeight) || ChartHEIGHT);

            jQuery("#f-width").keyup(function(e) {
                clearTimeout(timeOut);
                if (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10) {
                    jQuery.cookie(cookey.fWidth, jQuery("#f-width").val());
                    resizeChart('flash');
                } else {
                    timeOut = setTimeout(function() {
                        jQuery.cookie(cookey.fWidth, jQuery("#f-width").val());
                        resizeChart('flash');
                    }, DELAY);
                }
            });

            jQuery("#f-height").keyup(function(e) {
                clearTimeout(timeOut);
                if (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10) {
                    jQuery.cookie(cookey.fHeight, jQuery("#f-height").val())
                    resizeChart('flash');

                } else {
                    timeOut = setTimeout(function() {
                        jQuery.cookie(cookey.fHeight, jQuery("#f-height").val())
                        resizeChart('flash');
                    }, DELAY);
                }
            });

            jQuery("#j-width").keyup(function(e) {
                clearTimeout(timeOut);
                if (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10) {
                    jQuery.cookie(cookey.jWidth, jQuery("#j-width").val())
                    resizeChart();
                } else {
                    timeOut = setTimeout(function() {
                        jQuery.cookie(cookey.jWidth, jQuery("#j-width").val())
                        resizeChart();
                    }, DELAY);
                }
            });

            jQuery("#j-height").keyup(function(e) {
                clearTimeout(timeOut);
                if (e.which == 13 || e.keyCode == 13 || e.which == 10 || e.keyCode == 10) {
                    jQuery.cookie(cookey.jHeight, jQuery("#j-height").val())
                    resizeChart();
                } else {
                    timeOut = setTimeout(function() {
                        jQuery.cookie(cookey.jHeight, jQuery("#j-height").val())
                        resizeChart();
                    }, DELAY);
                }
            });


            function resizeChart(type) {
                var fHeight = jQuery.cookie(cookey.fHeight) || ChartHEIGHT,
                    fWidth = jQuery.cookie(cookey.fWidth) || ChartWIDTH,
                    jHeight = jQuery.cookie(cookey.jHeight) || ChartHEIGHT,
                    jWidth = jQuery.cookie(cookey.jWidth) || ChartWIDTH,
                    hContainer = jQuery('#hc-chart-container').parent(),
                    rContainer = jQuery('#red-chart-container-div').parent(),
                    fContainer = jQuery('#flash-chart-container').parent();

                    // flash or js
                if (type == 'flash') {

                    HChart && HChart.resizeTo && HChart.resizeTo(fWidth, fHeight);
                    !/\%/.test(fHeight) && fContainer.height(fHeight);
                    !/\%/.test(fWidth) && fContainer.width(fWidth);
                    !/\%/.test(fHeight) && hContainer.height(fHeight);
                    !/\%/.test(fWidth) && hContainer.width(fWidth);
                } else {
                    RChart && RChart.resizeTo(jWidth, jHeight);
                    !/\%/.test(jHeight) && rContainer.height(jHeight);
                    !/\%/.test(jWidth) && rContainer.width(jWidth);
                }
            }
            // End change height and widht
            $('#chart-type').chosen();
            $('#chart_type_chzn').css('width', '350px');
            $('#chart_type_chzn .chzn-drop').css('width', '348px');
            $('#chart_type_chzn .chzn-search').css('width', '310px');
            $('#chart_type_chzn .chzn-search input').css('width', '100%');

            $('.chzn-button').hover(function() {
                $(this).toggleClass('chzn-container-active');
            });
        });

        $('#data-forconvert').click(function (e) {
            var $dataTextArea = jQuery('#chartxml'),
                isItJSON = isJSON($dataTextArea.val());

            $dataTextArea.val(isItJSON ? chartDataXML : JSON.stringify(chartDataJSON, 0, 4));

            if (e.shiftKey) $('#submit').click();
        });
        // End document ready function


    };




$(document).ready(function() {


    $("#cachedChartTypesDIV").css({ display: jQuery.cookie('cachedChartTypesVisible')==='true' ? 'block': 'none'});
    $("#showhideCachedChartTypesDIV").removeClass().addClass(
           $("#cachedChartTypesDIV").is(":visible") ?  'showminus' : 'showplus'
    );

    $("#showhideCachedChartTypesDIV").unbind('click').click( function () {
        $("#cachedChartTypesDIV").toggle();
        var isVisible = $("#cachedChartTypesDIV").is(":visible");
        jQuery.cookie('cachedChartTypesVisible', isVisible);
        $("#showhideCachedChartTypesDIV").removeClass().addClass(
            isVisible ? 'showminus' : 'showplus'
        );
    });


    var $frame = document.getElementById("hcChartContainer"),
        interval;

    $($frame.contentDocument).ready(function () {

        interval = setInterval(function () {
            if ($frame.contentWindow.chart) {
                interval = clearInterval(interval);
                window.HChart = $frame.contentWindow.chart;
                initUI();
            }
        }, 500);
    });





});




if (jQuery.cookie('enablefirebug') === 'true') {
    document.write('<script type="text/javascript" src="../../source/libraries/firebug/firebug-lite.js"> { openEnabled: true;} <\/script>');
}


// sample testing /// Added by Pallab in index.html, ported here by Sudipto on 25 dec 2012
jQuery(function(){
    var selector = $('#chart-type'),
        allSamples = sampleDatas,
        chartData,
        chartType,
        smokeTestDiv = document.getElementById('smoke-test-area'),
        chartXMLTextArea = document.getElementById('chartxml'),
        sampleElements = [],
        i,
        ln,
        elem;

    selector.change(function(){
        chartType = selector[0].value.toLowerCase(),
        chartData = allSamples[chartType];

        //remove existing elements
        smokeTestDiv.innerHTML = "";
        chartData && showSamples(chartData);
    });

    function showSamples(data){
        var i,
            btn,
            text,
            btnText = "",
            ln = data.length,
            obj;

            for(i = 0; i < ln; i += 1){
                obj = {};
                btn = obj.btn = document.createElement('BUTTON');
                btn.setAttribute('id', "btn_"+i);
                btn.setAttribute('class', 'chzn-container chzn-container-single chzn-button');
                btn.counter = i;
                btn.onclick = function(){
                    chartXMLTextArea.value = typeof data[this.counter] !== "string" ?
                                (data[this.counter]['xml'] || data[this.counter][1] || data[this.counter].toString()) :
                                data[this.counter];
                    $('#submit').click();
                };


                text = obj.text = document.createTextNode( typeof data[i] !== "string" ?
                        (data[i].text || data[i][0]||"") :
                        "Sample "+(i+1));
                btn.appendChild(text);
                smokeTestDiv.appendChild(btn);
                sampleElements.push(obj);
            }
    }

});

// end of sample testing